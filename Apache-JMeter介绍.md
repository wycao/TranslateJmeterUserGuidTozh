# [Apache-JMeter介绍](https://jmeter.apache.org/index.html)

**Apache JMeter™**是一个开源应用软件， 100%纯Java应用，可以用于接口功能测试和性能测试。最初设计时，主要是用于测试 Web 应用程序，但具有自扩展能力。

## JMeter能做什么？

Apache JMeter可以用于测试静态和动态资源的性能，web动态应用程序。可以用它模拟在不同负载情况下，服务器、服务器组、网络等对象的性能情况。

Apache JMeter的功能包括，但不限于：

+ 能够测试不同应用程序、服务、协议的性能：
  + web的HTTP\HTTPS协议
  + soap\rest 服务
  + FTP服务
  + JDBC协议
  + LDAP
  + JMS的消息中间件
  + 邮件的SMTP\POP3\IMAP协议
  + shell命令
  + TCP协议
  + java对象
+ 功能齐全的IDE，可以通过浏览器、应用程序，快速录制、构建和调试测试脚本。
+ 强大的CLI命令行模式，可以在任何兼容Java程序的操作系统上进行性能测试。
+ 完整的动态HTML报告。
+ 可以轻松的从HTML、JSON、XML或任何文件中，提取想要的数据，进行关联。
+ 可以在java中100%移植。
+ 完整的多线程框架，可以灵活的实现单线程和多线程的测试。
+ 测试结果可以缓存也可以离线重放和分析。
+ 高可扩展的核心：
  + 通过插件，扩展取样器
  + 在取样器中可以通过JSR223、Groovy、Beanshell进行程序语言化扩展
  + 可视化的数据分析插件
  + 函数可以为测试提供动态数据和动态数据输入
  + 通过maven、gradle、jenkins可以轻松实现与第三方库的持续集成



## 注意：JMeter不是浏览器

JMeter不是浏览器，它是在协议级别上工作。虽然看起来像个浏览器，但是，JMeter不会执行浏览器支持的所有操作。如HTML页面中js、css等。不会像浏览器那样呈现HTML页面。