# 15.[JMeter属性配置](https://jmeter.apache.org/usermanual/properties_reference.html#introduction)

JMeter属性，主要是 jmeter.properties 和 reportgenerator.properties 文件中配置的属性信息。修改这些属性配置，应该写到user.properties文件中。修改属性配置之后，要重启JMeter。

## 15.1 JMeter语言属性

+ language   JMeter的GUI界面语言，默认注释掉了，使用缺省配置en，英文。这个属性，是唯一一个必须在jmeter.properties文件中修改的，不能再user.properties文件中配置。
+ language.add   该属性中显示了JMeter支持的所有语言。 zh_CN 是简体中文。

## 15.2 SSL属性

系统ssl属性，现在位于system.properties文件中，JMeter不再在 javax.xxxx 属性写到 jmeter.properties 文件中。系统属性，必须在system.properties文件中定义。

+ https.sessioncontext.shared		默认为false， 在ssl会话上，不共享上下文
+ https.default.protocol           默认TLS， https的默认加密认证方式，可以修改为 SSLv3
+ https.socket.protocols          启用的协议列表
+ https.cipherSuites                 可以在HTTPS中使用的SSL密码套件，用逗号分隔。
+ httpclient.reset_state_on_thread_group_iteration         在启动新线程组是重置HTTP状态。默认 true
+ httpclient.reset_state_on_thread_group_iteration       在迭代之间重置SSL上下文缓存
+ https.keyStoreStartIndex       启用索引，默认0
+ https.keyStoreEndIndex      约束索引，默认0

## 15.3 GUI外观配置

+ jmeter.laf.windows_10     WIN10系统UI
+ jmeter.loggerpanel.display     显示logger面板，默认false
+ jmeter.loggerpanel.maxlength     logger面板的最大行数，默认1000， 0表示不限制
+ jmeter.gui.refresh_period       GUI界面刷新频率，单位毫秒， 默认500

## 15.4 GUI工具栏配置

+ jmeter.toolbar.icons       工具栏图标
+ jmeter.toolbar              工具栏列表
+ jmeter.toolbar.icons.size      工具栏图标大小
+ jmeter.icons                JMeter图标
+ jmeter.tree.icons.size    JMeter树图标大小
+ jmeter.hidpi.mode    JMeter的HiDPI模式
+ jmeter.hidpi.scale.factor      HiDPI比例因子
+ not_in_menu        不显示的菜单
+ gui.quick_X        GUI的快捷键配置

## 15.5 JMX备份配置

+ jmeter.gui.action.save.backup_on_save      在GUI界面中，启动自动保存jmx文件功能
+ jmeter.gui.action.save.backup_directory      在GUI界面中自动保存jmx文件的文件夹
+ jmeter.gui.action.save.keep_backup_max_hours        设置备份保留的时长，默认没有设置，永久保留。
+ jmeter.gui.action.save.keep_backup_max_count        设置最大保留份数，默认保留10份。
+ save_automatically_before_run       在运行测试计划之前，保存

## 15.6 远程主机和RMI配置

+ remote_hosts     远程主机，多个之间用逗号分隔
+ server_port     认证服务端口
+ client.rmi.localport       使用的 RMI 端口的参数 默认值为 0
+ client.try     尝试初始化远程引擎的次数，默认 1
+ client.retries_delay      尝试重新初始化远程引擎的时间间隔
+ client.continue_on_fail      初始化重连失败处理
+ server.rmi.port       分布式服务端口，默认1099
+ server.rmi.create    启动时创建RMI认证
+ server.exitaftertest       测试退出之后
+ server.rmi.ssl.keystore.type     用于RMI连接的密钥类型，默认jks
+ server.rmi.ssl.keystore.file        密钥文件  默认 rmi_keystore.jks
+ server.rmi.ssl.keystore.password     密钥的密码，默认 changeit
+ server.rmi.ssl.keystore.alias     密钥的别名,  默认 RMI
+ server.rmi.ssl.truststore.type    受信任的密钥类型   JKS
+ server.rmi.ssl.truststore.file      受信任的秘密文件 默认  rmi_keystore.jks
+ server.rmi.ssl.truststore.password   受信任的密钥存储到密码  默认 changeit
+ server.rmi.ssl.disable        不启动加密认证传输，默认 flase， 大多时候要设置为true

## 15.7 Apache HTTPClient 通用属性

+ http.post_add_content_type_if_missing       如果没有消息头，会自动添加一个消息头，在JMeter4.1版本以前是自动添加 Content-Type: application/x-www-form-urlencoded  现在默认为false
+ httpclient.timeout         http连接超时时长， 0表示无超时，默认为0
+ httpclient.version       设置http版本，默认1.1

+ httpclient.socket.http.cps      将http每秒字符数设置为大于零的值以模拟慢速连接。默认值 0
+ httpclient.socket.https.cps     将https每秒字符数设置为大于零的值以模拟慢速连接。默认值 0
+ httpclient.loopback      启用循环协议 ，默认 true
+ httpclient.localaddress     主控机的ip
+ http.proxyUser        http代理的用户名
+ http.proxyPass        http代理的密码

## 15.8 HTTPClient4 属性配置

+ hc.parameters.file         覆盖 Apache HttpClient 参数的属性文件， 默认 hc.parameters
+ httpclient4.auth.preemptive      使用基本身份验证时抢先发送授权标头  默认 true
+ httpclient4.retrycount      尝试的重试次数 ，默认0
+ httpclient4.request_sent_retry_enabled      重试幂等和非幂等请求， 默认flase
+ httpclient4.idletimeout     空闲连接超时，单位 毫秒，默认 0
+ httpclient4.validate_after_inactivity      检查从上次连接之后的时长， 默认4900
+ httpclient4.time_to_live    TTL的时长，单位毫秒， 默认60000
+ httpclient4.deflate_relax_mode       忽略 EOFException 信号，默认 false
+ httpclient4.gzip_relax_mode        忽略EOFException信号指定的GZiped， 默认false
+ httpclient4.default_user_agent_disabled     是否默认ua头，默认 false

## 15.9 HTTP 缓存管理器配置

+ cacheable_methods      可以缓存的方法的空格或逗号分隔列表，默认GET
+ cache_manager.cached_resource_mode      缓存资源的模式， 默认RETURN_NO_SAMPLE
+ RETURN_CUSTOM_STATUS.code    RETURN_CUSTOM_STATUS时的返回码，默认空
+ RETURN_CUSTOM_STATUS.message      RETURN_CUSTOM_STATUS时的消息，默认空

## 15.10 结果文件配置

+ jmeter.save.saveservice.output_format    JMeter保存文件的格式，支持xml,csv,db，默认csv
+ jmeter.save.saveservice.assertion_results_failure_message       是否保存断言失败信息到csv文件
+ jmeter.save.saveservice.assertion_results      断言结果，有效值none,first,all。 默认 none
+ jmeter.save.saveservice.data_type       是否保存 data_type ，默认true
+ jmeter.save.saveservice.label       是否保存label，默认true
+ jmeter.save.saveservice.response_code     是否保存响应码， 默认true
+ jmeter.save.saveservice.response_data     是否保存响应体，csv格式不支持，默认false
+ jmeter.save.saveservice.response_data.on_error     是否保存失败响应体，默认fasle
+ jmeter.save.saveservice.response_message      是否保存response_message，默认true
+ jmeter.save.saveservice.successful     是否保存成功信息，默认true
+ jmeter.save.saveservice.thread_name    是否保存线程名称， 默认ture
+ jmeter.save.saveservice.time     是否保存时间， 默认true
+ jmeter.save.saveservice.subresults   是否保存子取样器结果， 默认true
+ jmeter.save.saveservice.assertions     是否保存断言结果， 默认true
+ jmeter.save.saveservice.latency     是否保存latency时长，默认true
+ jmeter.save.saveservice.connect_time    是否保存connect_time时长， 默认false
+ jmeter.save.saveservice.samplerData    是否保存样本数据，默认false
+ jmeter.save.saveservice.responseHeaders     是否保存响应头， 默认false
+ jmeter.save.saveservice.requestHeaders       是否保存请求头， 默认false
+ jmeter.save.saveservice.encoding      是否保存编码， 默认false
+ jmeter.save.saveservice.bytes       是否保存字节大小， 默认true
+ jmeter.save.saveservice.url       是否保存URL地址， 默认false
+ jmeter.save.saveservice.filename     是否保存文件名，默认 false
+ jmeter.save.saveservice.hostname    是否保存主机名， 默认false
+ jmeter.save.saveservice.thread_counts   是否保存线程数量， 默认true
+ jmeter.save.saveservice.sample_count      是否保存样本数量， 默认false
+ jmeter.save.saveservice.idle_time        是否保存空闲时间 ， 默认true
+ jmeter.save.saveservice.timestamp_format       时间格式，默认ms
+ jmeter.save.saveservice.default_delimiter   csv格式的分隔符，默认 逗号
+ jmeter.save.saveservice.print_field_names      是否输出表头，只在csv格式时有效，默认true
+ jmeter.save.saveservice.base_prefix    文件的相对地址
+ jmeter.save.saveservice.autoflush    写入结果时，自动刷新，默认false
+ sampleresult.timestamp.start        保存开始时间戳， 默认false
+ sampleresult.useNanoTime     是否使用System.nanoTime()， 默认true
+ sampleresult.nanoThreadSleep     用一个后台进程计算时间偏移，默认5000

## 15.11 分布式请求结果处理模式配置

从JMeter2.9版本开始，在用分布式进行性能测试时，测试数据，默认mode模式是分批从服务器上返回的，这个mode模式可以修改，可选的模式有：

+ Standard   逐个返回样本结果
+ Batch      累积结果，分批返回
+ Statistical     只返回样本汇总统计概括信息
+ Stripped     与Standard模式类似，但是把响应从样本结果中剥离出来。
+ StrippedBatch   与Batch类似，但是，把取样结果删除了
+ Asynch  异步发送，
+ StrippedAsynch   与Asynch类似，但是从取样结果中去除了响应数据。
+ StrippedDiskStore  与DiskStore类似，但是，从取样结果中去除了响应数据

## 15.12 JDBC请求属性配置

+ jdbcsampler.nullmarker      空字符串，默认 "]NULL["
+ jdbcsampler.max_retain_result_size      JDBC保存的最大结果大小
+ jdbc.config.check.query        用于确定数据库是否仍在响应的查询列表
+ jdbc.config.jdbc.driver.class      JDBC的驱动类名列表

## 15.13 TCP取样器的属性配置

+ tcp.handler      默认 TCPClientImpl
+ tcp.eolByte      跳过eol检查的范围[-128, 127]
+ tcp.charset       默认字符集
+ tcp.status.prefix     状态码前缀
+ tcp.status.suffix      状态码后缀
+ tcp.status.properties      状态转换为属性的文件
+ tcp.binarylength.prefix.length     二进制文件前缀长度

## 15.14 CLI模式的聚合报告

+ summariser.name      聚合报告的名称
+ summariser.interval     聚合报告的时间间隔
+ summariser.log     聚合报告日志
+ summariser.out     聚合输出
+ summariser.ignore_transaction_controller_sample_result    忽略事务控制器生成的结果

## 15.15 后端监听器配置

+ backend_graphite.send_interval         发送给graphite的时间间隔，单位秒， 默认1秒
+ backend_influxdb.send_interval          发送给influxdb的时间间隔，单位秒，默认5秒
+ backend_influxdb.connection_timeout     连接influxdb的超时时长，单位毫秒， 默认1000毫秒
+ backend_influxdb.socket_timeout         influxdb的套接字超时读取时长，单位毫秒，默认3000毫秒
+ backend_influxdb.connection_request_timeout      influxdb超时请求时长，单位毫秒， 默认100毫秒

## 15.16 其他属性配置

+ resultcollector.action_if_file_exists    保存结果到文件时，如果文件存在，怎么处理。有三种：①ASK询问方式；②APPEND追加到文件末尾；③删除现有文件，新建文件。
+ jmeter.expertMode    打开\关闭专家模式
+ httpsampler.max_bytes_to_store_per_request     每个取样器结果存储在内存中的最大字节大小。
+ httpsampler.max_buffer_size     HTTP取样器的最大读缓存大小，默认66560byte
+ httpsampler.max_redirects     单个http取样器的最大重定向数量，默认20
+ httpsampler.max_frame_depth       单个http取样器的最大帧深度，默认5
+ httpsampler.ignore_failed_embedded_resources     忽略资源下载错误
+ httpsampler.parallel_download_thread_keepalive_inseconds    保持并行下载线程的活跃时间，默认60秒
+ httpsampler.embedded_resources_use_md5      不要保留嵌入的资源响应数据
+ httpsampler.user_defined_methods     自定义的其他HTTP方法
+ sampleresult.default.encoding      取样器的结果编码，默认ISO-8859-1
+ CookieManager.delete_null_cookies     是否删除空cookie， 默认 true
+ CookieManager.allow_variable_cookies    是否允许改变cookie， 默认 true
+ CookieManager.save.cookies    是否把cookie存储为变量， 默认 false
+ jmeterengine.threadstop.wait     等待线程停止时长， 单位 毫秒，默认5000
+ jmeterengine.remote.system.exit     退出时是否停止RMI， 默认 false
+ jmeterengine.stopfail.system.exit     在CLI模式时，停止线程是否调用System.exit(1) 退出， 默认 true
+ jmeterengine.force.system.exit     在CLI模式时，结束测试是否调用System.exit(0)退出， 默认false
+ jmeterengine.nongui.port      CLI模式，默认监听消息的端口，默认 4445
+ jmeterengine.nongui.maxport    CLI模式，监听消息的最大端口，默认4455
+ view.results.tree.max_results      查看结果树，最大显示数量，默认 500
+ view.results.tree.max_size       查看结果树最大大小， 默认 10485760
+ view.results.tree.max_line_size    查看结果树行的最大大小  默认 110000byte
+ jmeter.reportgenerator.apdex_satisfied_threshold     报告中APDEX的容忍值 单位毫秒，默认500
+ jmeter.reportgenerator.apdex_tolerated_threshold     报告中APDEX的最大可接受极限时长，单位毫秒，默认1500
+ jmeter.reportgenerator.sample_filter   报告中，过滤器
+ jmeter.reportgenerator.temp_dir        报告的临时文件夹
+ jmeter.reportgenerator.report_title       报告的标题， 默认 Apache JMeter Dashboard
+ jmeter.reportgenerator.overall_granularity       报告的取点时间间隔， 默认60000