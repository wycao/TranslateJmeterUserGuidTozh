# JMeter5.5中文帮助

[TOC]

# 01.[JMeter入门](https://jmeter.apache.org/usermanual/get-started.html)

## 1、Jmeter使用应遵循的过程

### 1.0.1 构造测试计划

使用GUI图形界面运行JMeter，然后，编写好测试脚本，点击 运行 > 启动，在查看结果树或其他监听器中查看运行结果。

### 1.0.2 进行性能测试

测试计划做好了之后，就可以开始测试。第一步，要保证JMeter的运行环境，这与其他任何测试工具是一样的：

+ 提供CPU、内存、网络方面的资源
+ 操作系统调整
+ 确保安装了Java运行环境
+ 增加JMeter的堆大小。 默认时，JMeter的堆大小为1GB，这可能不满足你的测试要求，你可以根据你的测试计划中要运行的线程数进行适当的调整。

一切准备好后，就可以使用CLI模式进行性能测试。 **注意：不要使用GUI图形界面进行性能测试！**

使用CLI模式，你可以将测试结果生成到csv或xml格式的文件中，在测试结束时，再生成HTML报告。默认时，JMeter会在运行过程中，显示性能测试摘要报告，你也可以在测试过程中，把实时数据写入后端监听器。

### 1.0.3 分析性能测试结果

进行性能测试后，在HTML报告中，可以分析测试结果。



## 1.1 JMeter环境要求

### 1.1.1 JAVA运行环境

JMeter兼容java8及以上的版本，虽然只要有JRE就能运行，但是，建议安装JDK，JMeter需要使用JDK中的keytool应用程序。

### 1.1.2 操作系统

JMeter是一个100%的java应用程序，能运行java程序的操作系统，就能运行JMeter，所以，你的操作系统必须具备运行java的能力。



## 1.2  JMeter可选环境要求

### 1.2.1 SSL加密

JMeter的HTTP取样器，默认协议级为TLS，可以在jmeter.properties 或 user.properties文件中对 https.default.protocool属性进行配置来更改。

JMeter HTTP取样器，配置为接受所有证书，无论是否受信任，也无论有效期为多长。这是为了在测试时提供最大的灵活性。

### 1.2.2 JDBC驱动程序

如果要测试JDBC协议，则需要将数据库提供商提供的JDBC驱动jar包放到JMeter的lib目录中。

### 1.2.3 ActiveMQ JMS程序

如果你要测试JMS程序，则需要将activemq-all-xxxx.jar包，放到JMeter的lib目录中。



## 1.3 JMeter安装

要安装JMeter，你需要下载JMeter官方提供的zip或tar包，解压到任何你希望放置的路径。前提是你的系统正确安装了JDK\JRE，并且配置好了JAVA_HOME环境变量。



## 1.4 运行JMeter

在windows系统中，运行bin文件夹下的‘jmeter.bat’文件，在linux或mac系统中，运行bin文件夹下的‘jmeter’文件，过一会，就会出现JMeter的GUI界面。

> GUI模式，只能用于创建测试脚本，性能测试必须使用CLI模式

在JMeter的bin文件夹下，还有很多可能会用上的文件。.CMD的文件，是在windows2000或更高的系统中使用的。

+ jmeter.bat  默认时运行图形界面JMeter 
+ jmeterrw.cmd  在没有windowsshell控制台的情况下运行JMeter图形界面
+ jmeter-n.cmd  以CLI无图形界面模式运行跟在后面的jmx文件
+ jmeter-n-r.cmd  以CLI无图形界面模式运行所有分布式机器，执行跟在后面的jmx文件
+ jmeter-t.cmd 以GUI图形界面模式加载跟在后面的jxm文件
+ jmeter-server.bat 启动jmeter-server服务
+ mirror-server.cmd  以CLI无图形界面模式运行JMeter镜像服务器
+ shutdown.cmd  关闭远程分布式服务，以正常停止CLI无图形界面实例
+ stoptest.cmd   快速关闭远程分布式服务，以停止CLI无图形界面实例

也可以通过自定义一些文件来做配置。如，在JMeter的bin目录中新建一个setenv.bat文件，文件中添加：

```bash
set JVM_ARGS=-Xms1024m -Xmx1024m -Dpropname=value
```

运行该文件时，JVM_ARGS会覆盖jmeter.bat脚本中JVM设置，并在启动JMeter时，使用该配置。

可以自定义的环境变量有：

+ DDRAW：   JVM选项来影响raw的使用。如：` -Dsum.java2d.ddscale=true` 默认时为空。
+ GC_ALGO：   JVM的GC垃圾回收配置，默认值为  `-XX:+UseG1GC -XX:MaxGCPauseMillis=250 -XX:G1ReservePercent=20`

+ HEAP：  启动JMeter时的JVM内存设置，默认为  `-Xms1g -Xmx1g -XX:MaxMetaspaceSize=256m`
+ JMETER_BIN：  Jmeter的bin目录，在setenv.bat中，可以自定义。
+ JMETER_COMPLETE_ARGS：  如果配置，则只有JVM_ARGS 和JMETER_OPTS有效，其他配置均失效。默认为空。
+ JMETER_HOME：  安装目录，将从这个路径中获取jmeter.bat文件
+ JMETER_LANGUAGE：  指定运行时的语言，默认为  `-Duser.language="en" -Duser.region="EN"`
+ JM_LAUNCH：  java.exe或javaw.exe的文件配置
+ JVM_ARGS：启动JMeter时的java配置项，会添加到java命令中。



linux或mac环境中的文件：

+ jmeter  运行jmeter的图形界面模式
+ jmeter-server  启动jmeter服务模式
+ jmeter.sh   jmeter运行脚本
+ mirror-server.sh   在CLI无图形界面模式运行JMeter镜像服务
+ shudown.sh   正常停止CLI无图形界面远程分布式服务
+ stoptest.sh    突然停止CLI无图形界面的远程分布式服务

可以在jmeter 或jmeter.sh文件中，设置一些JMeter的JVM配置项，如

```shell
JVM_ARGS="-Xms1024m -Xmx1024m" jmeter -t test.jmx 
```

  这个命令，将覆盖JMeter默认的堆配置。

如果想要永久的修改这些配置，可以在JMeter的bin文件夹下创建setenv.sh文件，这个文件，在执行`jmeter`命令时，会覆盖原来的配置。如：

```sh
export HEAP="-Xms1G -Xmx1G -XX:MaxMetaspaceSize=192m"

export JMETER_LANGUAGE=" "
```



### 1.4.1 JMeter的类路径

JMeter会自动从以下目录中查找jar文件：

+ JMETER_HOME/lib    JMeter的jar文件
+ JMETER_HOME/lib/ext  JMeter的组件和插件

如果你自己开发了JMeter的组件，你应该把它打成jar文件，并复制jar文件到JMeter的lib/ext目录中。JMeter讲自动找到这些组件。**请不要将JMeter的依赖jar包或应用程序的jar包，放到lib/ext目录中，这个目录，仅用于放置JMeter的组件或插件。**

如果你不想把JMeter的插件包放在lib/ext目录中，你也可以在jmeter.properties中定义search_paths属性，指定路径。

JMeter的应用程序jar包和依赖jar包，应该放在lib目录中。如果你不想放在lib目录中，你也可以在jmeter.properties文件中定义User.classpath属性或palugin_dependency_paths属性，指定路径。

> JMeter只会找到.jar文件，不会去找.zip文件。



### 1.4.2 从模板创建测试计划

JMeter可以通过模板创建测试计划。

![JMeter-1.4.2-1.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-1.4.2-1.jpg)

你可以点击菜单栏中的  模板... 图标，在弹窗中，选择一个模板，然后，点击 Create 创建。

有些模板，需要输入参数。完成参数填写后，点击 验证按钮，将从模板创建测试计划。



### 1.4.3 使用代理命令

JMeter提供代理服务器能力，可以在jmeter或jmeter.bat命令后面增加一些代理参数：

+ -E 要使用的代理方案
+ -H 代理服务器的主机名或ip地址
+ -P 代理服务器的端口
+ -N 非代理主机
+ -u 代理身份的用户名
+ -a 代理身份的密码

如： `jmeter -E https -H my.proxy.server -P 8000 -u username -a password -N localhost`

你也可以使用 --proxyScheme、 --proxyHost、 --proxyPort、 --username、--password作为参数名称。

如果使用了代理服务，则JMeter会自动生成以下新的系统属性：

+ http.proxyScheme
+ http.proxyHost
+ http.proxyPort
+ https.proxyHost
+ https.proxyPort
+ http.proxyUser
+ http.proxypassword
+ http.nonProxyHosts
+ https.nonProxyHosts



### 1.4.4 CLI无图形界面模式

性能测试时，你必须在CLI无图形界面模式中，才能获得最佳测试结果。CLI命令参数有：

+ -n  指定JMeter在cli模式下运行
+ -t  包含测试计划的jmx文件名称
+ -l  将运行结果记录到jtl文件的名称
+ -j  JMeter运行的日志文件名称
+ -r 使用在jmeter.properties文件中配置的“remote_hosts”属性指定的服务运行测试
+ -R 指定远程分布式服务进行测试
+ -g  把csv格式的文件，生成HTML仪表盘报告
+ -e  将测试结果转换为HTML报告
+ -o  将生成到报告输出到文件夹。 文件夹必须为空或不存在。
+ -H  指定代理服务器的主机名或IP地址
+ -P  指定代理服务器的端口

如： `jmeter -n -t my_test.jmx -l log.jt -H my.proxy.server -P 8000`

> 译者添加：
>
> CLI执行性能测试，可能会出现无法无法退出问题，可以在jmeter.properties配置文件中设置：
>
> jmeterengine.remote.system.exit=true
>
> jmeterengine.stopfail.system.exit=true
>
> jmeterengine.force.system.exit=true



### 1.4.5服务模式

JMeter分布式，需要在所有的分布式节点上以服务的模式运行JMeter，然后通过GUI图形界面控制分布式服务，也可以通过CLI无图形界面模式控制分布式节点。启动服务，请在每台机器上运行`jmeter-server`（`jmeter-server.bat`）

如果希望在测试结束后退出JMeter服务，可以在jmeter.properties文件中配置server.exitafertext=true。

在CLI无图形界面模式命令中，运行分布式测试，可以向分布式机器传递参数

+ -G 定义在分布式服务中设置JMeter属性

+ -X 定义在测试结束时，退出服务
+ -Rserver1，server2  可以替换`-r`参数，来指定分布式服务列表，而不用重新定义remote_hosts

如： `jmeter -n -t testplain.jmx -r [-Gprop=val] [-Gglobal.properties]`



### 1.4.6 通过命令行覆盖属性

JMeter的系统属性和JMeter属性，可以在命令行中通过添加参数来设置，而不修改jmeter.properties文件。

+ -D[prop_name]=[value]   定义JMeter的系统属性
+ -J[prop_name]=[value]  定义JMeter属性
+ -G[prop_name]=[value]   定义要发送给所有分布式服务的JMeter属性
+ -G[propertyfile]  定义一个包含所有要发送给所有分布式服务的JMeter属性的文件
+ -L[category]=[priority] 设定JMeter日志级别

如：`jmeter -Duser.dir=dirPath -Jremote_hosts=127.0.0.1 -Ljmeter.engine=DEBUG`



### 1.4.7 JMeter命令帮助

| 参数                                    | 意思                                   |
| --------------------------------------- | -------------------------------------- |
| --？                                    | 打印命令选项参数                       |
| -h, --help                              | 打印使用帮助                           |
| -v, --version                           | 打印版本信息                           |
| -p, --profile <argument>                | 指定使用的JMeter属性文件               |
| -q, --addprop <argument>                | 添加多个JMeter属性文件                 |
| -t, --testfile <argument>               | 指定运行的测试计划                     |
| -l, --logfile <argument>                | 取样结果写入文件                       |
| -i, --jmeterlogconf <argument>          | JMeter日志配置文件                     |
| -j, --jmeterlogfile <argument>          | JMeter运行日志写入文件                 |
| -n, --nongui                            | 使用无图形界面运行JMeter               |
| -s, --server                            | 运行JMeter服务                         |
| -H, --proxyHost <argument>              | 设置JMeter代理服务器                   |
| -P, --proxyPort <argument>              | 设置JMeter代理服务端口                 |
| -N, --nonProxyHosts <argument>          | 设置非代理服务列表                     |
| -u, --username <argument>               | 设置JMeter代理服务的账号               |
| -a, --password <argument>               | 设置JMeter代理服务的密码               |
| -J, --jmeterproperty <argument>=<value> | 定义新的JMeter属性                     |
| -G, --globalproperty <argument>=<value> | 定义新的全局属性                       |
| -D, --systemproperty <argument>=<value> | 定义新的系统属性                       |
| -S, --systemPropertyFile <argument>     | 添加系统属性文件                       |
| -f, --forceDeleteResultFile             | 强制删除已经存在的测试结果文件和文件夹 |
| -L, --loglevel <argument>=<value>       | 设定日志级别                           |
| -r,--runremote                          | 运行所有remote_hosts服务               |
| -R, --remotestart <argument>            | 指定运行远程服务                       |
| -d, --homedir <argument>                | 指定JMeter的home文件夹                 |
| -X, --remoteexit                        | 在结束测试时退出远程服务               |
| -g, --erportonly <argument>             | 从结果文件中生成聚合报告               |
| -e, --reportatendofloadtexts            | 在测试结束后生成聚合报告               |
| -o, --reportoutputfloader <argument>    | 报告面板输出到文件夹                   |



## 1.5 JMeter配置

如果要修改JMeter的运行属性，可以修改bin目录中的user.properties文件或创建jmeter.properties副本。

JMeter命令中，如果指定属性配置，执行顺序为：

1、-p 属性文件

2、加载jmeter.properties文件

3、-j 日志文件

4、日志初始化

5、加载user.properties配置

6、加载system.properties配置

7、处理其他命令行选项参数



# 02.[JMeter测试计划](https://jmeter.apache.org/usermanual/build-test-plan.html)

JMeter测试计划，是描述JMeter在运行时讲要执行的一系列的步骤。一个完整的测试计划将由一个或多个线程组、逻辑控制器、取样器、监听器、计时器、断言和配置元件等组成。

## 2.1 添加或删除元件

选择测试计划树中一个元件，然后右键，从右键列表中选择添加一个新元件。

要删除某个元件，请先确保元件已被选中，然后右键，选择删除。

## 2.2 加载和保存元件

在测试计划树中，右键选择 “将所选内容另存为...” 可以保存所选元件及旗下的所有子元件。

## 2.3 保存测试计划

保存测试计划，不是必须的，但是，建议先保存，再运行。

## 2.4 运行测试计划

运行测试计划，可以通过菜单栏 运行 > 启动，快捷键`ctrl+r`。当JMeter运行时，在菜单栏的最右边，将显示一个绿色图标。此时，在运行菜单中， “启动” 菜单不可用，“停止” 菜单可用。

在这个绿色团标的左侧数字是 活跃线程数/总线程数。这些数字，只包括本地测试时的线程数数据，不包括分布式时，远程机器上的线程数。

## 2.5 停止测试

菜单中有两个停止命令：

+ stop停止，快捷键 `ctrl + .`  尽可能快的停止线程。许多测试请求时可以被中断的，可以提前终止。stop停止命令，就会在 jmeterengine.threadstop.wait配置的时间内(默认5秒)，停止线程。如果没有停止，就会给出一个提示信息。stop停止命令，可以反复执行，如果执行失败，则需要关闭JMeter来进行清理。
+ shutdown关闭 快捷键`ctrl + ,` 当线程的请求完成时，就停止线程，不会中断任何活跃的线程请求。这样，可能会在点击关闭之后，比较长时间还没有停止。

在CLI模式下运行JMeter，没有菜单，也没有快捷键功能，因此，JMeter的cli模式是监听特定的端口上的命令。默认时是 jmeterengine.nongui.port参数配置的4445端口，JMeter支持在默认端口被占用时自动选择其他端口，jmeterengine.nongi.maxport参数配置了最大端口4455，可以在这十个端口范围内自动选择。

选择的端口，将在命令窗口的控制台中显示。

通过这个端口，可以发送控制命令：

+ Shutdown  正常停止
+ StopTestNow 立刻停止

也可以通过执行bin目录中的 shutdonw.cmd 或shutdown.sh 和 stoptest.cmd 或stoptest.sh 命令来控制。但是，这个命令，必须在与运行的JMeter在同一台机器上，才有效。

## 2.6错误报告

JMeter运行时，会向jmeter.log文件中写入运行信息以及告警、错误信息。

在JMeter的图形界面菜单栏的右侧，有个三角形图标，图标的旁边数字显示告警和错误数量。点击图标，可以在图形界面窗口底部显示jmeter.log日志信息。只是偶尔可能会有一些无法捕获的错误记录会现在在命令控制台窗口中。如果测试的行为不符合您的预期，请检查日志信息。

取样器错误，如404，通常不会记录在日志文件中，这些会被存放在样本运行结果中。



# 03.[测试计划中的元件](https://jmeter.apache.org/usermanual/test_plan.html)

一个最小的测试脚本，将包括测试计划、线程组和一个或多个取样器。

## 3.0测试计划

在JMeter测试计划界面中，有一个“Functional Testing函数测试模式”，如果选中，则JMeter会把从服务器返回的数据写入到一个文件中。这在有些时候是很有用的。但是，在性能测试时，请不要勾选。



## 3.1 线程组

线程组元件，使所有测试的起点，所有的控制器、取样器等都必须挂在线程组下面。有些元件，如监听器，可以直接放在测试计划下面，这时，监听器将应用于所有线程组。

线程组，控制JMeter执行测试时的线程数，线程组，可以配置：

+ 线程数
+ ramp-up时长
+ 执行测试的次数

每个线程都会独立完整的完成测试计划中的内容。多线程时，用于模拟对服务器进行并发请求。

ramp-up时长，告诉JMeter用多长时间创建出设定的线程数。如果线程数为10，ramp-up为100秒，则JMeter需要用100秒时间启动10个线程，每个线程启动10后，下一个线程才启动。

默认情况下，JMeter线程组配置每个线程遍历一次旗下的元件。

线程组还允许设定线程生存周期。点击线程组的循环次数的复选框，启用或禁用额外配置。接下来，你可以配置持续运行时长 和 启动延迟时间，来控制每个线程组的持续运行时间和启动后的秒数。



## 3.2控制器

Jmeter有两种类型的控制器： 取样器和逻辑控制器。这些驱动测试的处理。

取样器，告诉JMeter把请求发送给被测服务器。如，添加HTTP取样器，就可以使用JMeter发送HTTP协议请求。

逻辑控制器，自定义JMeter的使用逻辑，决定何时发送请求。

### 3.2.1 取样器

取样器告诉JMeter将请求发送到服务器并等待响应。它们安装脚本树结构中的顺序执行，控制器，用于控制取样器的重复执行次数。

JMeter取样器包括：

+ FTP取样器
+ HTTP取样器（可用于SOAP 、REST 服务请求）
+ JDBC请求
+ Java请求
+ JMS请求
+ JUnit单元测试请求
+ LADP请求
+ Mail请求
+ OS操作系统进程请求
+ TCP请求

每个取样器都有多个可以配置的属性，可以通过在测试计划中添加一个或多个配置元件进行进一步配置。

如果要发送相同类型的多个请求到同一个服务器，请考虑使用默认配置元件

在测试计划下添加监听器，可以查看或存储请求结果到磁盘。

如果你想验证JMeter执行请求的响应，可以将断言添加到取样器下面。

### 3.2.2逻辑控制器

逻辑控制器，你可以自定义JMeter何时发送请求的逻辑。逻辑控制器可以改变请求的执行顺序。可以通过逻辑控制器，实现重复请求。

了解逻辑控制器对测试计划的影响，可以参考如下结构树：

+ 测试计划
  + 线程组
    + 仅一次控制器
      + HTTP登录请求
    + HTTP请求搜索页面
    + 交替控制器
      + HTTP取样器搜索“A”
      + HTTP取样器搜索“B”
      + HTTP请求默认值(配置元件)
    + HTTP请求默认值(配置元件)
    + cookie管理器(配置元件)

在这个测试模板中，HTTP登录请求，将只会执行一次，后续的迭代中，将跳过。

登录后，将执行 HTTP请求搜索页面，这是一个非常简单的请求，没有任何的逻辑控制器控制。

加载搜索请求后，我们要进行搜索，实际上，我们的搜索期望是，加载搜索页面，搜索“A”，再加载搜索页面，搜索“B”，通过使用交替控制器，就可以实现预期。使用交替控制器，它不会随机传递子取样器，而使按顺序传递。

在交替控制器中，有一个HTTP请求默认值，作用于两个搜索请求。

再下面有一个HTTP请求默认值，就作用于整个线程组。

最后一个cookie管理器，在任何的web测试中，建议添加cookie管理器，否则，JMeter将忽略cookie。在线程组下添加cookie管理器，可以保证整个线程组共享相同的cookie。



## 3.3 监听器

监听器提供在JMeter运行时，对测试结果信息的访问。图形结果监听器，用图形绘制响应时间。查看结果树监听器，显示取样器的请求和响应信息。其他监听器则提供摘要和聚合信息。

监听器，可以将测试数据指定到文件，以供后续使用。JMeter中每个监听器都提供了一个字段，来指定存储数据的文件位置，还有一个配置按钮，可以选择保存的字段信息，和保存时用csv还是xml格式。

> 注意：所有的监听器保存的数据都是相同的，只是数据在监听器中显示的方式不一样而已。



## 3.4 定时器

默认情况下，JMeter线程按顺序执行而不停止。

添加定时器，将导致JMeter在每个计时器之前延迟一定的时间，再执行取样器。



## 3.5 断言

断言，允许你判断从服务器返回的结果是否符合预期。

您可以向任何取样器添加断言。

> 注意： 若要断言某个取样器的执行结果，请将断言添加为该取样器的子级

失败的断言会添加到查看结果树视图中，并计入错误率。



## 3.6 配置元件

配置元件与取样器密切配合，虽然它不发送请求，但是，它可以修改请求。

> 注意： 配置元件中的 用户定义的变量，无论放在何处，都会在测试开始时进行处理。



## 3.7 预处理器

预处理器是在发出取样器请求之前执行的某些操作。如果预处理器添加在取样器下作为子集，则在该取样器运行之前执行。



## 3.8 后置处理器

后置处理器在发出去取样器请求之后，对响应进行某些操作。



## 3.9 元件执行顺序

JMeter元件的执行顺序：

0. 配置元件
1. 预处理器
2. 定时器
3. 取样器
4. 后置处理器
5. 断言
6. 监听器

> 注意： 计时器、断言、预处理器、后置处理器，仅在有取样器的时候，才有效。
>
> 逻辑控制器和取样器，按照它们在脚本树中出现的顺序处理。
>
> 其他元件，工具它们的作用范围和元件的类型进行处理。



## 3.10 元件作用域

JMeter的脚本树试有分层和有序的。脚本中的某些元件（监听器、配置元件、后置处理器、预处理器、断言、计时器）是有严格分层的，而另一些元件（逻辑控制器、取样器）是有序的。当你创建一个测试计划脚本树时，就已经确定了元件的执行顺序和作用范围。



## 3.11 属性和变量

JMeter属性在jmeter.properties中定义的。属性对于JMeter而言，是全局的。属性可以通过jmeter的函数，实现在测试计划中被引用。

JMeter变量，是每个线程的。每个线程的值可能相同，也可能不同。如果变量值由某个线程更新，则仅更改该线程中变量的值。

在测试计划中定义的用户定义变量，和配置元件中的用户定义的变量，可以用于整个测试计划。如果一个变量被多个地方定义，则最后一个才有效。启动脚本运行时，初始设置的变量，将复制给所有的线程。这些仅适用于当前线程组中的线程。

setProperty函数，可以定义JMeter属性，这是可以作用于整个测试计划的。因此，可以在不同线程组的线程之间传递。

> 变量名和 属性名，都是严格区分大小写的。



## 3.12 参数化脚本

用户定义的变量，被定义之后，值将不会被改变。因此，你可以把它用于测试计划中，经常出现的表达式的简写，或运行过程中保持不变的项。

在构建测试计划的时候，就标记出哪些项在运行过程中是不变的，哪些项是可能发生改变的。然后按照一定的规则定义变量名，区分它们。还要考虑哪些项是线程的私有变量，区别定义变量名。

在测试计划中，引用变量的时候，使用${变量名}的方式引用。在大型测试时，也可以用属性来定义变量的值。



# 04.[开发HTTP协议脚本](https://jmeter.apache.org/usermanual/build-web-test-plan.html)

## 4.1 添加用户

JMeter测试计划，第一步是添加线程组，线程组告诉JMeter要模拟的用户数量，每个用户发送请求的频率，以及总共要发送多少请求。

右键测试计划，点击 添加 > 线程组，添加线程组。

接下来需要修改默认配置，在窗口的右侧显示了线程组的控制面板。

![JMeter-4.1-1.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-4.1-1.jpg)

可以自己设置一个名称；设置一个线程数数量；设置启动所有线程数的ramp-up时长。循环次数配置要重复执行的次数，如果循环次数为1，则JMeter的每个线程都只运行一次就结束。若想让JMeter重复运行，可勾选‘永远’复选框。

## 4.2 添加HTTP请求默认值

选择线程组，右键，然后选择 添加 -> 配置元件 -> HTTP请求默认值。

![JMeter-4.2-1.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-4.2-1.jpg)

添加 服务器名称或IP， 在整个测试计划下，所有的HTTP请求，都将收到这个相同的配置。

## 4.3 添加cookie管理器

对于web测试而言，都应该添加cookie管理器，除非你的应用程序不使用cookie。在线程组下添加cookie管理器，所有的HTTP请求，都将共享cookie。

选中线程组，右键，点击 添加 -> 配置元件 -> cookie管理器。

![JMeter-4.3-1.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-4.3-1.jpg)

## 4.4 添加HTTP取样器

在JMeter中，HTTP取样器按照在脚本数中出现的顺序发送请求。

选中线程组， 右键， 添加 -> 取样器 -> HTTP请求

![JMeter-4.4-1.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-4.4-1.jpg)

## 4.5 添加监听器

查看结果树，将HTTP请求的所有结果存储到文件，并可视化的呈现数据。

## 4.6 使用相同或不同用户

线程组中，默认勾选了 "每次迭代使用相同用户"。

![JMeter-4.6-1.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-4.6-1.jpg)

这样，在整个脚本运行过程中，同一个线程不管迭代多少次，都会使用相同的用户身份。

去掉这个勾选，然后在 HTTPcookie管理器、HTTP缓存管理器、HTTP授权管理器中，勾选“Use Thread Group configuration to control clearing”实现不同用户。

![JMeter-4.6-1-2.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-4.6-1-2.jpg)

![JMeter-4.6-2.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-4.6-2.jpg)

![JMeter-4.6-3.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-4.6-3.jpg)

![JMeter-4.6-4.jpg](images/JMeter-4.6-4.jpg)



# 05.[开发JDBC协议脚本](https://jmeter.apache.org/usermanual/build-db-test-plan.html)

要构建JDBC测试计划，需要把数据库驱动jar包，放到JMeter的lib目录中。

## 5.1 添加线程组

每个JMeter测试计划的第一步是添加线程组。线程组，告诉JMeter要模拟多用户数量，多久发送一次请求， 总共要发起多少情况。

测试计划，右键， 添加 -> 线程组

## 5.2 添加JDBC请求

添加JDBC请求之前，添加JDBC连接配置， 选择测试计划，右键，添加 -> 配置元件 -> JDBC连接配置

![JMeter-5.1-1.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-5.1-1.jpg)

+ Variable Name for create pool 绑定连接池的变量名。 这是唯一标识配置。JDBC取样器将使用这个标识。

+ Database URL 数据库网址  jdbc:mysql://ipofserver:3306/databasename

+ JDBC Driver class   JDBC驱动类，选择 com.mysql.jdbc.Driver
+ Username  数据库的用户名
+ Password  数据库的密码

其他配置字段，可以使用默认值。

在JDBC connection configuration中配置了一个数据库连接池，这个连接池的名称，在JDBC请求中将被引用，这个变量名必须唯一。

选中线程组，右键，添加 -> 取样器 -> JDBC Request。然后，选择此元件，查看控制面板。

![JMeter-5.2-1.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-5.2-1.jpg)

## 5.3 添加监听器

添加一个监听器，用于将JDBC请求的结果进行展示或存储在一个文件中。



# 06.[开发SOAP协议脚本](https://jmeter.apache.org/usermanual/build-ws-test-plan.html)

## 6.1 创建测试计划

在模板中选择 “构建SOAP服务计划”，然后“创建”

![JMeter-6.1-1.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-6.1-1.jpg)

改变以下内容：

+ 在 HTTP请求默认值 中，更改 服务器名称或IP
+ 在 SOAP请求 中，更改 路径。

接下来，在 HTTP消息够管理器 中更新 “SOAPAction”， 在某些webservice中，可能不需要SOAPAction。

最后一步，就是将正确的SOAP消息粘贴到 消息体数据 文本框中。

## 6.2 添加用户

线程组，是告诉JMeter要模拟多并发用户数量和发送请求的频率。

## 6.3 添加监听器

监听器，负责将HTTP请求的所有结果进行可视化呈现和存储结果到文件。

# 07.[开发JMS协议脚本]()



https://jmeter.apache.org/usermanual/build-jms-point-to-point-test-plan.html



https://jmeter.apache.org/usermanual/build-jms-topic-test-plan.html



# 08.[取样器](https://jmeter.apache.org/usermanual/component_reference.html#samplers)

## 8.1 取样器

取样器是执行JMeter的实际工作。每个取样器都会生成一个或多个取样结果。执行的结果包括多种属性（成功\失败、经过的时长、数据量大小等），都可以在监听器中查看。

## 8.2 HTTP取样器

此取样器允许您将HTTP\HTTPS请求发送到被测服务器。它还能控制JMeter解析一些HTML资源或其他嵌入式资源，并对这些资源进行检索。

检索的资源包括：

+ 图片images
+ 小程序applets
+ 样式文件css和从这些文件中引用的资源
+ 外部脚本
+ 框架frames或嵌入框架iframes
+ 背景图片
+ 背景声音

默认的解析器是：org.apache.jmeter.protocol.http.parser.LagartoBasedHtmlParser  也可以通过修改属性配置htmlparser.className来修改。

如果要向同一个被测服务器发送多个请求， 请考虑使用 HTTP请求默认值 元件，使用它，你可以不必为每个HTTP取样器输入相同的元素。

或者，当您的HTTP请求中包含了很多参数时，您也可以使用 HTTP测试记录器来创建。

JMeter有三种不同的元件来定义HTTP协议取样器：

+ AJP/1.3取样器
  + 使用tomcat的mod_jk协议(一种允许在AJP模式下测试tomcat，无需Apache httpd服务)，AJP取样器不支持多文件上传。

![JMeter-8.2-2.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-8.2-2.jpg)

+ HTTP请求

  + 它有一个下拉框，可以选择HTTP协议的实现。
    ![JMeter-8.2-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-8.2-1.jpg)
    + Java  使用JVM提供HTTP实现。
    + HTTPClient4  使用 Apache HttpComponents HttpClient 4.x.
    + 空白  不设置实现，此时，依赖HTTP请求默认值中的设置或jmeter.properties中定义的jmeter.httpsampler属性。

+ GraphQL HTTP请求

  ![JMeter-8.2-3.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-8.2-3.jpg)

  + 这是HTTP请求的GUI图形界面变体，以提供更方便的UI元素查看或编辑GraphQL-Query、GraphQL-Variables、GraphQL-OperationName。
    + 请求方法：只有POST和GET，这符合GraphQL over HTTP规范。默认为POST
    + 参数和请求体：您可以通过Query、Variables和Operation Name，这些界面元素进行编辑。


如果请求需要使用代理服务器或代理登录授权，您就需要添加 HTTP授权管理器， 对于表单登录，你需要弄清楚表单提交的按钮动作，选择正确的HTTP请求方法，并填写正确的表单参数。

每个线程都单独使用SSL上下文，如果要使用单个SSL上下文，就需要修改JMeter属性： https.sessioncontext.shared=true

默认情况下，从5.0 版本开始，SSL 上下文在线程组迭代期间保留，并在每次测试迭代时重置。 如果在测试计划中同一用户多次迭代，则应将其设置为 false。

httpclient.reset_state_on_thread_group_iteration=true

JMeter 默认为 SSL 协议级别的 TLS。如果使用了其他不同的级别，可以修改属性 https.default.protocol配置。

JMeter 还允许通过更改属性https.socket.protocols来启用其他协议。

如果请求使用了cookie，可以添加 HTTPcookie管理器，如果有多个取样器都要使用cookie或授权，可以把cookie管理器和授权管理器添加到线程组下，这样，整个线程组就可以共享。

**HTTP请求元素：**

| 字段名                            | 描述                                                         | 必填 |
| --------------------------------- | ------------------------------------------------------------ | ---- |
| 名称                              | 取样器的名称，会显示在脚本树中                               | 否   |
| 服务器                            | 服务器名称或IP地址，不要带http://。如果在消息头中添加了Host， 这个就是虚拟主机名。 服务器，是必填的。但是，也可以在http请求默认中配置，或在 路径 中包含完整的主机:端口 地址 | 是   |
| 端口                              | 服务侦听端口，默认为80                                       | 否   |
| 连接超时                          | 连接超时，等待连接打开的毫秒数                               | 否   |
| 响应超时                          | 响应超时，等待响应的毫秒数，这适用于每次请求，如果一个请求分几次，则是总体的响应时长。可以使用 持续时长断言 检查需要很长时间的响应 | 否   |
| 服务代理                          | 执行请求的代理服务器主机名或IP地址， 不包含 http:// 前缀     | 否   |
| 代理端口                          | 代理服务器侦听的端口                                         | 否   |
| 用户名                            | 代理服务器的用户名                                           | 否   |
| 密码                              | 代理服务器的密码                                             | 否   |
| 服务实现                          | 默认值由jmeter.httpsampler属性配置，如果没有，则使用HttpClient4 | 否   |
| 协议                              | HTTP HTTPS 或FILE， 默认HTTP                                 | 否   |
| 方法                              | GET POST HEAD TRACE OPTIONS PUT DELETE PATCH 不支持java实现，如果选中HttpClient4实现，还支持 COPY LOCK MKCOL MOVE PROPFIND PROPPATCH UNLOCK REPORT MACALENDAR SEARCH.  设置httpsampler.user_defined_methods 可以为HttpClient4配置默认方法。 | 否   |
| 内容编码                          | 内容编码，适用于PUT POST PATCH FILE方法，这是内容编码，与消息头中的编码无关 | 否   |
| 自动重定向                        | 将根据http请求处理自动跟踪重定向，它不会被JMeter看到，因此不会看到重定向的响应。仅限于GET HEAD请求。不能用于POST 和PUT请求。 | 否   |
| 跟随重定向                        | 默认。会在结果中显示重定向的结果。主取样器的响应时间包含了重定向请求的时间。 | 否   |
| keep-avilve                       | 保持活跃。勾选了，会在消息头中，自动添加 Connection:keep-alive 。 这个在默认的HTTP请求中，没有用，因为连接重置，不受此控制。但是，在Apache HttpComponents HttpClient实现中有用。 | 否   |
| 对POST方法使用multipart/form-data | 对POST请求，使用multipart/form-data 或 application/x-www-form-urlencoded | 否   |
| 与浏览器兼容的头                  | 在使用multipart/form-data 时，控制消息头中User-Agent信息     | 否   |
| 路径                              | 资源路径                                                     | 否   |
| 随请求一起发送到参数              | 请求体是参数或消息体数据，每个参数都有一个名称和值。使用这些请求体，要使用正确的请求方法，如果是GET或DELETE方法，就会把请求体跟在URL后面，如果是POST或PUT方法，就会单独发送请求体。如果使用POST方法发送multipart/form-data，就要按照规范。如果你不确定参数是否要进行URL编码时，最好是选择URL编码。如下这些，都是要进行URL编码的：①、ASSII字符 ②、非ASCII字符 ③、保留字符 如【$ & + / :  ; = ?  @】④、不安全字符 如【, < > # % ... 】 | 否   |
| 文件路径                          | 要发送到文件名称。如果留空，则不发送文件，如果填写JMeter会自动将请求分为 multipart/form-data 表单请求发送。当MIME类型为空时，JMeter会猜测文件类型。 | 否   |
| 参数名                            | 请求的参数名称                                               | 否   |
| MIME类型                          | MIME类型。 如果是POST PUT PATCH请求，而且省略了name参数，则这个字段的值将作为Content-Type消息头。 | 否   |
| 从HTML中检索所有嵌入资源          | 告诉 JMeter 解析 HTML 文件 并为文件中引用的所有图像、Java 小程序、JavaScript 文件、CSS 等发送 HTTP/HTTPS 请求 | 否   |
| 将响应结果保存为MD5 hash？        | 如果选中此项，响应结果将不会显示在结果树中，而是被计算存储在MD5值中，这个主要用于响应数据很大时。 | 否   |
| 网站匹配                          | 正则匹配输入的网址信息                                       | 否   |
| 网站排除                          | 正则匹配 过滤掉 输入的信息                                   | 否   |

**GraphQL请求元素：**

| 字段名   | 描述                                    | 必填 |
| -------- | --------------------------------------- | ---- |
| 查询     | GraphQL查询语句                         | 是   |
| 变量     | json请求体                              | 否   |
| 操作名称 | 请求多操作文档时可选的 GraphQL 操作名称 | 否   |

**参数请求体：**

对应POST和PUT方法，如果不是发起文件，会通过连接参数来创建请求体。请注意，在不添加行尾结束符时，会串联参数。可以使用__char()函数来转换参数内容，这样就可以让参数为任意内容。如果勾选了编码，会对值进行urlencoded编码。

当请求体不是参数时，可以使用 消息体数据这个选项卡 来编写请求体。此请求体，可以编写：

+ GWT RPC 请求
+ JSON REST请求
+ XML REST请求
+ SOAP请求

注意： 参数 和 消息体数据  两个选项卡 互斥。

**请求方法：**

GET DELETE POST PUT PATCH方法的工作方式类似。但从JMeter3.1版本开始，只有POST方法，支持multipart/form-data和文件上传。PUT和PATCH方法的请求体要是如下几种：

+ 请求体没有名称的参数
+ 使用消息体数据
+ 使用文件上传，参数名称为空，请求参数写在文件中，MIME类型填写Content-Type值。

GET DELETE POST方法，可以使用 参数 来传递请求体； GET DELET PUT PATCH 方法，需要有Content-Type，可以在消息头管理器中添加Content-Type。

**模拟慢速连接：**

HttpClient4 和Java 实现，支持模拟慢速连接

```properties
# Define characters per second > 0 to emulate slow connections
#httpclient.socket.http.cps=0
#httpclient.socket.https.cps=0
```

但是，Java 实现仅支持慢速HTTPS连接。

**响应大小计算：**

Java实现， 是不包括传输时间的。

HttpClient4实现，包括响应体的传输时间。

**重试处理：**

默认情况下，HttpClient4和java实现，重试次数为0，对于HttpClient4，可以通过修改属性 httpclient4.retrycount来设置重试次数。HttpClient4实现，默认是在HTTP请求幂等上进行重试，可以通过属性 httpclient4.request_sent_retry_enabled=true来配置。  java实现，也可以通过修改属性 http.java.sampler.retries来设置重试次数。

## 8.3JDBC请求

此取样器是用JMeter向数据库发起JDBC请求。

在使用它之前，我们先需要配置 JDBC连接配置。

### 8.3.1 JDBC Connection Configuration

池化数据库连接配置，否则每个JDBC线程都将自己创建连接。

选择一个元件，右键，添加 -> 配置元件 -> JDBC Connection Configuration

![JMeter-8.3-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-8.3-1.jpg)

| 字段名                                    | 描述                                                         | 必填 |
| ----------------------------------------- | ------------------------------------------------------------ | ---- |
| 名称                                      | 元件在脚本树中显示的名字                                     | 否   |
| Variable Name for created pool            | 线程池变量名，一个池子创建多个连接。如果有多个jdbc配置元件使用了相同线程池名称，只会保留一个。 | 是   |
| Max Number of Connections                 | 线程池允许的最大连接数。大多数情况下，设置为0，意思是每个线程都可以从池中获得自己的连接，连接不在线程间共享。如果你真的想共享，那就把这个数值设置大于运行时的线程数，这样运行时线程就不会相互等待。 | 是   |
| Max Wait (ms)                             | 最大连接时长，超过这个时长的连接，就会报错。                 | 是   |
| Time Between Eviction Runs (ms)           | 运行线程之间的休眠时长。如果没有设置，就不会清除空闲线程。默认1分钟 | 是   |
| Auto Commit                               | 打开True或关闭False自动提交                                  | 是   |
| Transaction isolation                     | 事务隔离级别                                                 | 是   |
| Pool Prepared Statements                  | 池化准备。 -1 禁用池化， 0 表示预准备的语句数量不受限制      | 是   |
| Preinit Pool                              | 初始化连接池， False不初始化，在运行时初始化。               | 否   |
| Init SQL statements separated by new line | 初始化sql语句集合，用换行符分隔初始化语句。这些语句，只在连接配置的时候执行一次。 | 否   |
| Test While Idle                           | 空闲时测试                                                   | 是   |
| Soft Min Evictable Idle Time(ms)          | 非强制限制在连接被逐出连接池前可处于空闲状态的最短时间。     | 是   |
| Validation Query                          | 验证查询。用于确定数据库是否仍在响应的简单查询。默认为jdbc驱动的 isValid() 方法。但不同的数据库也会有些不一样。<br />hsqldb     select 1 from INFORMATION_SCHEMA.SYSTEM_USERS<br />oracle     select 1 from dual<br />DB2      select 1 from sysibm.sysdummy1<br />MySQL \ MariaDB      select 1<br />SQL Server       select 1<br />PostgreSQL       select 1 <br />Ingres       select 1 <br />Derby       select 1 <br />H2        select 1 <br /> Firebird        select 1 from rdb$database  <br />Exasol        select 1 | 否   |
| Database URL                              | 连接数据库的字符串                                           | 是   |
| JDBC Driver class                         | 数据库连接驱动。需要把数据库的驱动jar包，拷贝到JMeter的lib目录中。预设的配置，可以修改jdbc.config.jdbc.driver.class属性配置。<br />![JMeter-8.3-2.jpg](images/JMeter-8.3-2.jpg) | 是   |
| Username                                  | 连接数据库的用户名                                           | 否   |
| Password                                  | 用于连接到密码                                               | 否   |
| Connection Properties                     | 连接属性，建立连接时设置的连接属性                           | 否   |

不同的数据库和JDBC驱动，需要不同的JDBC设置。DatabaseURL和JDBC Driver class 由JDBC的驱动jar包定义。

如果在使用JMeter进程JDBC测试过程中，出现  No suitable driver 的错误，可能的原因有：

+ 找不到驱动程序类。去修改驱动类
+ 有驱动类，但是，连接字符串错误。

如果数据库没有启动或不能访问，则JMeter会报  java.net.ConnectException 的错误。

| 数据库                                | DatabaseURL                                                  | DriverClass                                        |
| ------------------------------------- | ------------------------------------------------------------ | -------------------------------------------------- |
| MySQL                                 | jdbc:mysql://host[:port]/dbname                              | com.mysql.jdbc.Driver                              |
| PostgreSQL                            | jdbc:postgresql:{dbname}                                     | org.postgresql.Driver                              |
| Oracle                                | jdbc:oracle:thin:@//host:port/service  或者 jdbc:oracle:thin:@(description=(address=(host={mc-name})(protocol=tcp)(port={port-no}))(connect_data=(sid={sid}))) | oracle.jdbc.OracleDriver                           |
| Ingress (2006)                        | jdbc:ingres://host:port/db[;attr=value]                      | ingres.jdbc.IngresDriver                           |
| Microsoft SQL Server (MS JDBC driver) | jdbc:sqlserver://host:port;DatabaseName=dbname               | com.microsoft.sqlserver.jdbc.<br />SQLServerDriver |
| Apache Derby                          | jdbc:derby://server[:port]/<br />databaseName[;URLAttributes=value[;…]] | org.apache.derby.jdbc.<br />ClientDriver           |
| MariaDB                               | jdbc:mariadb://host[:port]/<br />dbname[;URLAttributes=value[;…]] | org.mariadb.jdbc.Driver                            |
| Exasol                                | jdbc:exa:host\[:port\]\[;schema=SCHEMA_NAME\][;prop_x=value_x] | com.exasol.jdbc.EXADriver                          |

### 8.3.2 JDBC请求

如果JDBC请求中，定义了变量名称来接受select返回的对应列的值，将会返回每个变量名称加下划线和数值接受对应的值。变量名称加下划线加#号，为总数量。

![JMeter-8.3-3.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-8.3-3.jpg)

| 字段名                                                       | 描述                                                         | 必填 |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |
| 名称                                                         | 显示在脚本树中的元件名称                                     | 否   |
| Variable Name of Pool declared in JDBC Connection Configuration | 在JDBC连接池中配的配置的池名称。这里的变量名，必须和连接配置中的池名称一致。 | 是   |
| Query Type                                                   | 查询类型<br />Select Statement    用于查询数据<br />Updata Statement   用于更新数据<br />Callabel Statement  调用函数<br />Prepared Select Statement   带变量的查询数据<br />Prepared Update Statement   带变量的数据变更<br />Commit 提交<br />Rollback   回滚<br />Autocommit(false)  不自动提交<br />Autocommit(true)   自动提交<br />Edit 编辑<br /> Commit、Rollback、Autocommit(false)、Autocommit(true) 会忽略SQL，只改变连接状态 | 是   |
| SQL Query                                                    | sql语句。 不需要末尾的分号。不需要使用“{”“}”来封装语句。     | 是   |
| Parameter values                                             | 参数值，多个参数值之间用逗号分隔。使用“]NULL[”代表空值，也可以修改jdbcsampler.nullmarker这个属性来自定义。如果变量值中有逗号或双引号，则要用双引号引起来。 参数值要与脚本中的?号占位符数量一致。 | 是   |
| Parameter types                                              | 参数类型。多个参数时，用逗号分隔参数类型。一般使用VARCHAR，也支持其他JMeter中定义的类型。ARRAY、BIGINT、BINARY、BIT、BLOB、BOOLEAN、CHAR、CLOB、DATALINK、 DATA、 DECIMAL、 DISTINCT、 DOUBLE、 FLOAT 、INTEGER、 JAVA_OBJECT、 LONGVARCHAR、 NCHAR、 NCLOB、 NULL、 NUMERIC、 NVARCHAR、 OTHER、 REAL 、REF、 REF_CIRSPR、 ROWID、 SMALLINT、 SQLXML STRUCT、 TIME、 TIME_WITH_TIMEZONE、 TIMESTAMP、 TIMESTAMP_WITH_TIMEZONE、  TINYINT、 VARBINARY、 VARCHAR | 是   |
| Variable Names                                               | 接受结果的变量名称，多个变量名称之间用逗号分隔。变量名称数量要与返回结果列数量一致。 | 否   |
| Result Variable Name                                         | 包含所有结果的变量对象。如果此时要用变量的值需要写代码。如 colValue= vars.getObject("resultObject").get(0).get("colname"); | 否   |
| Query timeout(s)                                             | 查询超时时长，单位秒，空或0,表示无限，-1表示不设置查询超时时长 | 否   |
| Limit ResultSet                                              | 显示循环访问结果的行数。空或-1,表示无限制。                  | 否   |
| Handle ResultSet                                             | 结果处理。默认为StoreAsString 变量的所有结果都用字符串存储。StoreAsObject存储为对象。CountRecords计算记录。 | 否   |

## 8.4 JSR223取样器

JSR223取样器，编写JSR223脚本代理来创建、更新执行请求。

JSR223测试元件，可以显著提供脚本的性能。在下面情况下，可以显著提升性能：

+ 使用脚本文件，而是直接连接文件

+ 使用脚本文件，并勾选“缓存已编译的脚本”配置

  > 使用这个时候，主要脚本代码中不要使用JMeter变量或函数，因为，这个缓存只会缓存第一次打值。
  >
  > 勾选“缓存已编译的脚本”，这个功能执行引擎必须是JSR223可编译的，groovy语言可以，但是java、beanshell、JavaScript不是
  >
  > 如果使用groovy语言编写脚本，请设置JVM属性 -Dgroovy.use.classvalue=true, 不然可能会出现groovy内存泄露。

JSR223的缓存大小，由属性 jsr223.compiled_scripts_cache_size 配置。

![JMeter-8.4-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-8.4-1.jpg)

| 字段名                             | 描述                                                         | 必填 |
| ---------------------------------- | ------------------------------------------------------------ | ---- |
| 名称                               | 显示在脚本树中的元件名称                                     | 否   |
| Scripting Language                 | JSR223使用的脚本语言。通过下拉菜单选择不同的脚本语言         | 是   |
| Script File                        | JSR223脚本文件名称。如果使用相对路径，则相对地址为 user.dir属性配置地址 | 否   |
| Parameters                         | 参数。要传递给脚本文件或脚本中的参数列表                     | 否   |
| Cache compiled script if available | 是否缓存脚本语言。groovy语言支持。java\beanshell\javascript不支持。缓存将使用MD5hash。 | 否   |
| Script                             | 脚本                                                         | 是   |

如果有选择了脚本文件，则使用脚本文件，没有，则使用script中编写的脚本。

JMeter定义了一些方法可以在script脚本中直接使用：

+ log  日志方法
+ Label  取样器标签
+ FileName 文件名
+ Parameters  来自上面文件名中的参数
+ args   方法的参数
+ SampleResult 取样器结果
+ sampler 取样器
+ ctx  JMeter为文本内容
+ vars JMeter中定义的变量
+ props JMeter中定义的属性
+ OUT 系统输出信息

SampleResult取样器结果根据脚本的返回值设置，如果返回null空，则使用 SampleResult.setResponseData 直接设置响应，数据类型默认为text，也可以通过SampleResult.setDataType来设置为二进制。

SampleResult 变量为脚本提供了对所有字段的完全访问权限方法。

## 8.5 TCP取样器

TCP 取样器打开与指定服务器的 TCP/IP 连接。 然后，它发送文本，并等待响应。

如果选择“重用连接”，则在同一线程中的采样器之间共享连接， 前提是使用完全相同的主机名字符串和端口。 不同的主机/端口组合将使用不同的连接，不同的线程也是如此。 如果同时选择了“重用连接”和“关闭连接”，则在运行采样器后将关闭套接字。 在下一个采样器上，将创建另一个套接字。如果检测到错误 或未选择“重用连接”， 则关闭套接字。

以下属性可用于控制其操作：

+ tcp.status.prefix  tcp状态前缀
+ tcp.status.suffix   tcp状态后缀
+ tcp.status.properties   要将tcp状态转为小型属性的名称
+ tcp.handler tcp要处理的程序类名称， 处理的类名由界面指定， 如果没有指定，就在 org.apache.jmeter.protocol.tcp.sampler 中搜索。

也可以扩展类： org.apache.jmeter.protocol.tcp.sampler.TCPClient.  提供了以下实现：

+ TCPClientImpl  此实现是基础实现。读取响应时，会读取到字节末尾，或者是输入流的末尾。这由 tcp.eolByte这个属性配置。 可以通过tcp.charset属性来配置字符集。
+ BinaryTCPClientImpl  此实现是把十六进制内容转换为二进制，在读取响应时把二进制转为十六进制。末尾标志由 tcp.BinaryTCPClient.eomByte 配置决定。
+ LengthPrefixedBinaryTCPClientImpl    此实现，在二进制消息数据前加BinaryTCPClientImpl来扩展。长度前缀默认为2个字节。可以通过tcp.binarylength.prefix.length 这个属性来配置。
+ 超时连接配置  如果配置了，就在超时时终止。
+ 超时响应配置   如果配置了 tcp.status.prefix，就会在响应中，搜索这个配置值。如果找到，就是用这个作为响应码。  假如配置了 prefix="["  suffix="]" 就会在响应中搜索这之间的内容，作为响应内容。

响应码4xx、5xx 都会被视为失败请求。其他的，视为成功。

![JMeter-8.5-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-8.5-1.jpg)

| 字段名              | 描述                                                         | 必填 |
| ------------------- | ------------------------------------------------------------ | ---- |
| 名称                | 显示在脚本树中的元件名称                                     | 否   |
| TCPClient classname | TCPClient类名称。                                            | 是   |
| 服务器名称或IP      | TCP 服务器的名称或 IP                                        | 是   |
| 端口号              | 要使用的端口                                                 | 是   |
| Re-use connection   | 重用连接。 如果选中，连接保持打开状态，否则读取数据后关闭    | 是   |
| 关闭连接            | 如果选中，在运行后就关闭                                     | 是   |
| SO_LINGER           | 启用\禁用SO_LINGER，在创建套接字时以秒为单位，设置延迟时长。如果为0，可以防止在大量产生套接字时处于TIME_WAIT状态 | 否   |
| 行尾 （EOL） 字节值 | 行尾字节值，可以设置为[-128, 127]之间。也可以通过jmeter.properties文件中的eolByte属性配置。 | 否   |
| 超时连接            | 连接超时，单位毫秒， 0表示禁用                               | 否   |
| 超时响应            | 响应超时，单位毫秒，0表示禁用                                | 否   |
| 设置无延迟          | 设置无延迟                                                   | 是   |
| 要发送的文本        | 要发送的文本                                                 | 是   |
| 用户名              | 用户名                                                       | 否   |
| 密码                | 密码                                                         | 否   |

## 8.6 JMS取样器

### 8.6.1 JMS发布

JMS 发布服务器会将消息发布到给定的目标（主题/队列）。JMS本身不含有任何JMS实现的jar文件，需要自己下载jar文件放到lib目录中。

![JMeter-8.6-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-8.6-1.jpg)

| 字段名                           | 描述                                                         | 必填 |
| -------------------------------- | ------------------------------------------------------------ | ---- |
| 名称                             | 显示在脚本树中的元件名称                                     | 否   |
| use JNDI properties file         | 使用jndi.properites配置。                                    | 否   |
| JNDI Initial Context Factory     | 上下文信息                                                   | 否   |
| Provider URL                     | 提供者的URL地址                                              | 是   |
| Destination                      | 目的地，主题或队列名称                                       | 是   |
| Setup                            | 设置目标类型，At startup静态的；Each sample 动态的           | 是   |
| use Authentication               | 是否启用认证                                                 | 是   |
| User                             | 认证用户名                                                   | 否   |
| Password                         | 认证密码                                                     | 否   |
| Expiration                       | 消息过期时长，单位毫秒，未设置或0，表示永不过期              | 否   |
| Priority                         | 优先级，从低到高0-9，默认4                                   | 否   |
| Reconnect on error codes (regex) | 正则匹配错误代码后，再连接。空，则不会再连接                 | 是   |
| Number of samples to aggregate   | 要聚合的样本数                                               | 是   |
| Message source                   | 消息源<br />Text area 从文本对象获取消息源<br />From File 从应用文件获取消息源<br />Random File from folder specified below  来自指定文件夹中的随机文件，文件格式为.dat 或.txt | 是   |
| Message type                     | 消息类型， 文本、对象、消息对象、字节消息                    | 是   |
| 内容编码                         | RAW、DEFAULT、Standard charset                               | 是   |

对于 MapMessage 类型，JMeter 将源读取为文本行。 每行必须有 3 个字段，用逗号分隔。 这些字段是：

+ Name of entry 条目名称
+ Object class name 对象类名
+ Object string value  对象字符串值

ActiveMQ发布的实现：

| 字段名                            | 值                                                           |
| --------------------------------- | ------------------------------------------------------------ |
| Initial Context Factory上下文信息 | org.apache.activemq.jndi.ActiveMQInitialContextFactory       |
| Provider URL 提供者地址           | vm://localhost<br />vm:(broker:(vm://localhost)?persistent=false) |
| Queue Reference队列               | dynamicQueues/QUEUENAME                                      |
| Topic Reference 主题              | dynamicTopics/TOPICNAME                                      |

### 8.6.2 JMS订阅

订阅消息。

![JMeter-8.6-2.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-8.6-2.jpg)

| 字段名                           | 描述                                                         | 必填 |
| -------------------------------- | ------------------------------------------------------------ | ---- |
| 名称                             | 显示在脚本树中的元件名称                                     | 否   |
| use JNDI properties file         | 使用jndi.properties属性配置。                                | 是   |
| JNDI Initial Context Factory     | jndi上下文名称                                               | 否   |
| Provider URL                     | JMS提供者的地址                                              | 否   |
| Destination                      | 消息目标                                                     | 是   |
| Durable Subscription ID          | 持久订阅的ID                                                 | 否   |
| Client ID                        | 客户端标识，使用持久订阅消息的客户端ID，多线程时，ID要动态变化 | 否   |
| JMS Selector                     | JMS选择器                                                    | 否   |
| Setup                            | 目标类型设置。At startup静态的；Each sample 动态的           |      |
| use Authentication               | 是否启用认证                                                 | 是   |
| User                             | 认证用户名                                                   | 否   |
| Password                         | 认证密码                                                     | 否   |
| Number of samples to aggregate   | 要聚合的样本数                                               | 是   |
| Client                           | 客户，MessageConsumer.receive() 每个消息都掉receive()接收；MessageListener.onMessage() 建立一个侦听器，把所有消息用队列方式存储，完成后，侦听器处于活跃状态。 | 是   |
| Stop between samples?            | 在样本间是否停顿。                                           | 否   |
| Separator                        | 分隔符                                                       | 否   |
| Reconnect on error codes (regex) | 正则匹配错误代码后，再连接。空，则不会再连接                 | 否   |
| Pause between errors (ms)        | 发生错误时暂停多少毫秒                                       | 否   |

### 8.6.3 JMS点对点

JMS点对点取样器，可以实现消息的发送和接收，通常用于处理事务。

request_only   通常用于在 JMS 系统上放置负载。
request_reply  当您要测试处理发送到请求队列的消息的 JMS 服务的响应时间时，将使用request_reply，因为此模式将等待此服务发送的回复队列上的响应。
browse   返回当前队列深度，即队列中的消息数。
read   从队列中读取消息（如果有）。
clear   清除队列，即从队列中删除所有消息。

JMeter 使用属性 java.naming.security.[principal|credentials] 配置队列超时设置。如果不需要，可以配置 JMSSampler.useSecurity.properties这个属性为false。

![JMeter-8.6-3.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-8.6-3.jpg)

| 字段名                                       | 描述                                                         | 必填 |
| -------------------------------------------- | ------------------------------------------------------------ | ---- |
| 名称                                         | 显示在脚本树中元件的名称                                     | 否   |
| QueueConnection Factory                      | 用于连接到消息传递系统的JNDI 名称                            | 是   |
| JNDI Name Request queue                      | JNDI请求队列名称                                             | 否   |
| JNDI Name Reply queue                        | JNDI接收队列名称。如果此处提供了一个值，并且通信方式为 RequestResponse，则监听此队列的响应。 | 否   |
| Number of samples to aggregate               | 要聚合的样本数                                               | 是   |
| JMS Selector                                 | 由 JMS 规范定义的消息选择器，仅提取 遵循选择器条件的消息     | 否   |
| Communication style                          | 沟通方式<br />Request Only  只会发送消息，不会监听回复<br />Request Response  监听发送和接收消息。取决于JNDI队列的值。如果JNDI有回复队列的值，那么监听结果。如果JNDI的接受队列值为空，则只用于请求和服务器之间的通信。<br />Read 从监听器中读取消息。<br />Browse 确认队列深度，返回队列的消息数，不从队列中删除消息。<br />Clear 清除消息队列。 | 是   |
| Use alternate fields for message correlation | 使用备用字段进行消息关联。<br />Use Request Message ID  如果选中，将使用请求的JMSMessageID，否则使用请求的JMSCorrelationID.<br />Use Response Message ID 如果选中，将使用响应JMSMessageID，否则将使用响应JMSCorrelationID<br />JMS Correlation ID模式： 在关联ID上匹配请求和响应<br />JMS Message ID模式： 将请求消息ID和响应消息ID关联匹配 | 是   |
| Timeout                                      | 回复消息的超时时长，单位毫秒。默认职位2000ms， 0代表不限制   | 是   |
| Expiration                                   | 消息的过期时长，单位毫秒。默认0，永不过期                    | 否   |
| Priority                                     | 消息的优先级，低到高为[0,9]，默认为4                         | 否   |
| Use non-persistent delivery mode?            | 是否使用非持久交付模式                                       | 是   |
| Content                                      | 消息体                                                       | 否   |
| JMS Properties                               | JMS属性。JMS 属性是特定于基础消息传递系统的属性。 您可以设置值的名称、值和类（类型）默认为String。 | 否   |
| Initial Context Factory                      | 初始上下文                                                   | 否   |
| JNDI properties                              | JNDI 属性是底层 JNDI 实现的特定属性。                        | 否   |
| Provider URL                                 | 消息提供者URL                                                | 否   |

# 09.[逻辑控制器](https://jmeter.apache.org/usermanual/component_reference.html#logic_controllers)

逻辑控制器，控制取样器的执行顺序。

## 9.1 循环控制

如果添加了循环控制器，取样器将循环运行线程中的循环次数 × 循环控制器设定的次数。

添加了循环控制器，JMeter会用元件名称，创建一个公开索引 \_\_jm\_<元件名称>\_\_idx， 可以作为变量引用。假如控制的名称为LC，引用这个索引为 ${\_\_jm\_\_LC\_\_idx}，索引从0开始。

![JMeter-9.1-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-9.1-1.jpg)

| 字段名   | 描述                                                         | 必填 |
| -------- | ------------------------------------------------------------ | ---- |
| 名称     | 显示在脚本树中元件的名称                                     | 否   |
| 循环次数 | 控制子取样器每次迭代时循环执行的次数<br />-1 或勾选 永远，代表永远<br />必填 | 是   |



## 9.2 仅一次控制器

告诉JMeter，每个线程只运行一次。

![JMeter-9.2-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-9.2-1.jpg)



## 9.3  随机控制器

随机控制器，从其子取样器中随机选择一个执行。

![JMeter-9.3-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-9.3-1.jpg)

## 9.4 随机顺序控制器

随机顺序控制器，控制其子取样器随机顺序执行。

![JMeter-9.4-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-9.4-1.jpg)

## 9.5 吞吐量控制器

吞吐量控制器，控制器其子取样器执行频率，有两种模式：

+ 百分比   控制测试计划执行一定的百分比
+ 总执行次数  控制测试计划执行一定数量后停止执行

![JMeter-9.5-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-9.5-1.jpg)

| 字段名          | 描述                                                         | 必填 |
| --------------- | ------------------------------------------------------------ | ---- |
| 名字            | 显示在脚本树中元件的名称                                     | 否   |
| Execution Style | 执行风格，有TotalExecutions总执行次数 和 PercentExecutions百分比两种 | 是   |
| 吞吐量          | 一个数值。在百分比模式时，数值[0,100]之间。总执行次数模式时，数值控制将执行的总次数。 | 是   |
| PerUser         | 每个用户。如果选中，就是每个用户都按设置的执行。如果未选中，就是整个线程组按设置执行。 | 否   |

吞吐量控制器，不会严格控制吞吐量，严格控制吞吐量的是 Constant Throughput Timer常数吞吐量定时器。

![JMeter-9.5-2.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-9.5-2.jpg)

## 9.6 条件控制器

条件控制器，条件满足时，执行其下取样器。

![JMeter-9.6-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-9.6-1.jpg)

默认时选中将条件解释为变量表达式。

+ 条件框中，可以直接引入真、假
+ 条件框中，也可以使用 ${\_\_jexl3()}函数或${\_\_groovy()}函数运算结果为真、假。

如果取消 将条件解释为变量表达式，条件框将作为 JavaScript来解析，这样性能会有较大损失。

## 9.7 ForEach控制器

ForEach控制器循环变量一组相关变量的值。

![JMeter-9.7-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-9.7-1.jpg)

| 字段名               | 描述                             | 必填 |
| -------------------- | -------------------------------- | ---- |
| 名称                 | 显示在脚本树中元件的名称         | 否   |
| 输入变量前缀         | 输入变量的前缀，默认为空字符串   | 否   |
| 开始循环字段(不包含) | 循环变量的起始索引(不包含)       | 否   |
| 结束循环字段(包含)   | 循环变量的结束索引(包含)         | 否   |
| 输出变量名称         | 可以在取样器中直接引用的变量名称 | 否   |
| 分隔符               | 默认为“_”                        | 是   |

## 9.8 事务控制器

事务控制器，生成所有测试元件的总时间的元件。

> 如果选中了 Include duration of timer and pre-post processors in generated sample ，时间将包括所有的时间。

有两种模式：

+ 分别显示控制器和其子元件
+ 合并子元件

![JMeter-9.8-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-9.8-1.jpg)

事务控制器的时间，将包括所有取样器的时间。仅当所有取样器都成功，事务控制器才视为成功。

在有事务控制器合并子取样器时，生成的jtl文件中，csv格式时，不包括子取样器，xml格式时，包括子取样器。

| 字段名                                   | 描述                                                         | 必填 |
| ---------------------------------------- | ------------------------------------------------------------ | ---- |
| 名字                                     | 显示在脚本树中元件的名称                                     | 否   |
| 合并子取样器                             | 选中后，将合并所有的子取样器。否则，将独立所有的子取样器     | 是   |
| 在合并中包含前置处理器和后置处理器的时间 | 如果选中，将在事务控制器的时间中，包括计数器、前置处理器、后置处理器的时间。默认不选中 | 是   |

## 9.9 临界部分控制器

临界部分控制器，控制其子取样器之下时，只有一个线程锁运行。

![JMeter-9.9-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-9.9-1.jpg)

使用临界部分控制器后，多线程时，只能有一个线程被执行。

| 字段名 | 描述                               | 必填 |
| ------ | ---------------------------------- | ---- |
| 名称   | 显示在脚本树中元件的名称，         | 否   |
| 锁名称 | 锁名称，控制执行的时候使用的锁名字 | 是   |

# 10.[监听器](https://jmeter.apache.org/usermanual/listeners.html)

每个监听器都是从不同角度来展示测试结果。同时，也都提供了查看、保存、读取测试结果到文件的功能。

监听器的作用域是所有元件最后。

测试结果保存、读取功能是通用的，每个监听器都有一个将结果指定到文件和读取文件的功能。默认情况下，将结果指定存储为csv格式的jtl文件中。csv格式文件是最有效的，但是不如xml详细。

默认的csv格式的jtl文件，没有分析数据能力，但是，会把一些重要数据都按默认配置保存。使用CLI无图形界面模式生成的文件，也可以加载到监听器里面。可以使用“浏览”功能，选择打开文件。

如果要在加载新的文件，可以在菜单中选择 运行->清除(ctrl+E)或(ctrl+shift+E)

![JMeter-10.1-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.1-1.jpg)

| 字段名     | 描述                                                         | 必填 |
| ---------- | ------------------------------------------------------------ | ---- |
| 文件名     | 保存结果的文件名称。可以是相对路径或绝对路径。相对路径的起始点有两个，bin目录或jmx脚本路径。 | 否   |
| 浏览...    | 软件选择按钮                                                 | 否   |
| 仅错误日志 | 选择时，只显示\写入\读取错误结果                             | 否   |
| 仅成功日志 | 选择时，只显示\写入\读取成功结果                             | 否   |
| 配置       | 配置写入文件的格式和内容                                     | 否   |

### 配置

配置保存到jtl文件中的内容。

![JMeter-10.1-2.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.1-2.jpg)

名称后面带有CSV的，适合保存到csv格式的jtl文件中，带有XML的，适合保存到xml格式的jtl文件中。csv格式不能保存含换行符的内容。

## 10.1 图形结果

在性能测试时，请不要使用图形结果，因为它会消耗大量的内存、CPU资源。

图形结果监听器，会根据取样器的时间绘制一个简单的图形。

在图形中，用黑色标记当前取样器的时间数据，蓝色是平均响应时间，紫色是中位数时间，红色是标准差，绿色是吞吐量

![JMeter-10.1-3.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.1-3.jpg)

吞吐量数字表示服务器处理的实际请求数/分钟。此计算 包括您添加到测试中的任何延迟以及 JMeter 自己的内部处理时间。



## 10.2 查看结果树

“查看结果树”用树型结构显示所有取样器的执行结果。可以查看 任何样本的响应、整个请求所花费的时间、响应代码等。

![JMeter-10.2-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.2-1.jpg)

可以选择不同的渲染器，渲染响应结果。

| 渲染器                   | 描述                                                         |
| ------------------------ | ------------------------------------------------------------ |
| CSS选择器                | 仅使用于文本响应。                                           |
| Document                 | 文档视图将显示从各种文档中提取文本。文档视图，需要下载tika-app-x.x.jar包放到JMeter的lib文件夹中。 |
| HTML                     | HTML视图将响应呈现为HTML。                                   |
| HTML(download resources) | 选择后，渲染器将下载HTML代码中的资源。                       |
| HTML source formatted    | HTML源码格式视图                                             |
| JSON                     | json样式视图                                                 |
| JSON Path Tester         | Json路径测试器视图，可以用json-path表达式提取想要的数据。    |
| JSON JMESPath Tester     | JSON JMESPath视图，可以用JMESPath表达式提取想要的数据。      |
| Regexp Tester            | 正则表达式视图，仅适用于文本响应。可以通过正则表达式提取想要的数据。 |
| Text                     | 文本视图，仅当响应Content-Type为文本时，此操作才有效。Content-Type开头为 image/  audio/  video/ ，将被视为二进制，其他的为文本。 |
| XML                      | XML视图。                                                    |
| XPATH Tester             | XPATH测试视图，仅适应于文本响应。                            |
| 边界提取器               | 边界提取器视图，仅适用于文本响应。                           |

Scroll automatically?  选中后，自动滚动到最后一个响应。视图中显示默认显示500条，由view.results.tree.max_results 属性配置。

查找  可以搜索响应内容。

如果没有Content-Type，则内容面板中不会显示任何响应数据。

如果响应数据大于200K，则不会显示。可以修改view.results.tree.max_size配置来修改。

## 10.3 聚合报告

聚合报告会为每个事务创建一行，每行，汇总显示总的请求次数、最小响应时间、最大响应时间、平均响应时间、错误率、近似的吞吐量和吞吐率。

吞吐量是从发起请求开始计时的，它包括了请求的所有时间。如果一个事务中，包括了多个取样器和计时器，这将增加总时间，降低吞吐量的值。不同名称的两个相同取样器的吞吐量是两个相同名称的相同取样器的一半。所以，需要正确的使用取样器的名称。

计算中位数和90%、95%、99%的值，需要额外的内存。

![JMeter-10.3-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.3-1.jpg)

+ 标签  事务名称。 如果选中了“在标签中包含组名称？”，则将线程组名称添加为前缀。
+ #样本  事务在一段时间内的总请求次数
+ 平均值  一组数据的平均响应时间
+ 中位数  一组时间的中间响应时间。 50%的样本响应时间，不超过这个时间。
+ 90%     90%的请求的响应时间不超过这个时间。
+ 95%    95%的请求的响应时间不超过这个时间。
+ 99%   99%的请求的响应时间不超过这个时间。
+ 最小值   同一事务的所有时间中最短的时间
+ 最大值    最长时间
+ 异常率   出现错误的请求次数的占比
+ 吞吐量     平均每秒的样本数
+ 接受 KB/s   每秒接收多少千字节数据
+ 发送 KB/s   没秒发送多少千字节数据



## 10.4 表格中查看结果

用表格的形式显示每次请求的相关数据，需要使用大量内存。

![JMeter-10.4-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.4-1.jpg)

## 10.5 响应时间图

根据响应时间，绘制一个折线图

![JMeter-10.5-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.5-1.jpg)

## 10.6 汇总报告

与聚合报告类似，只是使用的内存更少。

![JMeter-10.6-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.6-1.jpg)

+ 标签  事务名称。 如果选中了“在标签中包含组名称？”，则将线程组名称添加为前缀。
+ #样本  事务在一段时间内的总请求次数
+ 平均值  一组数据的平均响应时间
+ 最小值   同一事务的所有时间中最短的时间
+ 最大值    最长时间
+ 标准偏差   响应时间的标准偏差
+ 异常率   出现错误的请求次数的占比
+ 吞吐量     平均每秒的样本数
+ 接受 KB/s   每秒接收多少千字节数据
+ 发送 KB/s   没秒发送多少千字节数据
+ 平均字节   每个样本的平均字节大小



## 10.7 保存响应到文件

此测试元素可以放置在测试计划中的任何位置。 对于其范围内的每个示例，它将创建一个响应数据文件。 它的主要用途是创建功能测试，但它也可以 在响应太大而无法显示在查看结果树侦听器中时很有用。 文件名是根据指定的前缀加上一个数字创建的。 文件扩展名是从文档类型（如果已知）创建的。 如果不知道，则文件扩展名设置为“未知”。 如果禁用了编号，并且禁用了添加后缀，则文件前缀为 作为整个文件名。这允许在需要时生成固定文件名。 生成的文件名存储在示例响应中，可以保存 如果需要，在测试日志输出文件中。

![JMeter-10.7-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.7-1.jpg)

| 字段名                                         | 描述                                                         | 必填 |
| ---------------------------------------------- | ------------------------------------------------------------ | ---- |
| 名字                                           | 显示在脚本树中元件的名称                                     | 否   |
| Save Successful Responses only                 | 仅保存成功的响应。                                           | 是   |
| Save Failed Responses only                     | 仅保存失败的响应                                             | 是   |
| Don't Save Transaction Controller SampleResult | 事务控制器生成的结果将被忽略                                 | 是   |
| Variable Name containing saved file name       | 要在其中保存生成的文件名的变量的名称。                       | 否   |
| 文件名称前缀                                   | 生成的文件名前缀，可以包含目录名称。文件名中不能有变量或线程组相关数据。 | 是   |
| Don't add number to prefix                     | 不要添加数值前缀                                             | 是   |
| Don't add content type suffix                  | 不要添加内容类型后缀                                         | 是   |
| Add timestamp                                  | 添加时间，yyyyMMdd-HHmm_ 后缀                                | 是   |
| Minimum Length of sequence number              | 序列号的最小长度。默认为0。如果没有选中 Don't add number to prefix，则自动添加数值0作为前缀 | 否   |

## 10.8 生成概要结果

添加生成概要结果后，在运行的调试窗口中会显示聚合报告。

![JMeter-10.8-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.8-1.jpg)

![JMeter-10.8-2.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.8-2.jpg)

![JMeter-10.8-3.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.8-3.jpg)



## 10.9 后端监听器

后端监听器是一个异步监听器，可用于插入后端监听器客户端的自定义实现。

![JMeter-10.9-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.9-1.jpg)

| 字段名         | 描述                                     | 必填 |
| -------------- | ---------------------------------------- | ---- |
| 名字           | 显示在脚本树中元件的名称                 | 否   |
| 后端监听器实现 | 后端监听器都实现类                       | 是   |
| 异步队列大小   | 异步处理样本结果是保存样本结果的队列大小 | 是   |
| 参数           | 后端监听器实现的参数                     | 是   |

**GraphiteBackendListenerClient实现：**

| 字段名                   | 描述                                                         | 必填 |
| ------------------------ | ------------------------------------------------------------ | ---- |
| graphiteMetricsSender    | org.apache.jmeter.visualizers.backend.graphite.TextGraphiteMetricsSender 或 org.apache.jmeter.visualizers.backend.graphite.PickleGraphiteMetricsSender | 是   |
| graphiteHost             | graphite的主机地址                                           | 是   |
| graphitePort             | graphite的端口                                               | 是   |
| rootMetricsPrefix        | 发送到后端的指标的前缀。默认是jmeter                         | 是   |
| summaryOnly              | 仅发送没有详细信息的摘要，默认true                           | 是   |
| samplersList             | 发送到后端的取样器名称，默认用分号分隔多个。                 | 是   |
| useRegexpForSamplersList | 是否使用正则匹配上一行的取样器名称，默认false，上一行用分号分隔。如果为true，上一行就是直接用正则匹配取样器名称。 | 是   |
| percentiles              | 发送给后端的百分位数                                         | 是   |

从JMeter3.2开始，新增了InfluxdbBackendListenerClient实现。

**InfluxdbBackendListenerClient实现**

![JMeter-10.9-2.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.9-2.jpg)

| 字段名                | 描述                                                         | 必填 |
| --------------------- | ------------------------------------------------------------ | ---- |
| influxdbMetricsSender | org.apache.jmeter.visualizers.backend.influxdb.HttpMetricsSender | 是   |
| indfluxdbUrl          | influxdb的地址 http://需要自己修改:8086/write?db=jmeter      | 是   |
| influxdbToken         | 在influxdb2 中新增的token令牌。 JMeter5.2开始才有。          | 是   |
| application           | 应用程序名称，自定义。                                       | 是   |
| measurement           | influxdb数据库名称，默认jmeter                               | 是   |
| summaryOnly           | 仅摘要信息                                                   | 是   |
| samplersRegex         | 正则匹配取样器名称                                           | 是   |
| percentiles           | 发送到后端的百分位数                                         | 是   |
| testTitle             | 测试名称                                                     | 是   |
| eventTags             | grafana中运行为注释添加标签。在influxdb数据库中作为一个标签存储事务。 | 否   |
| TAG_WhatEverYouWant   | 可以自己添加任意数量的标签。存储时会为这些标签添加TAG_开头   | 否   |

从JMeter5.4开始，增加InfluxDBRawBackendListenerClient实现，来兼容influxdb2。

**InfluxDBRawBackendListenerClient实现：**

![JMeter-10.9-3.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-10.9-3.jpg)

| 字段名                | 描述                                                         | 必填 |
| --------------------- | ------------------------------------------------------------ | ---- |
| influxdbMetricsSender | org.apache.jmeter.visualizers.backend.influxdb.HttpMetricsSender | 是   |
| influxdbUrl           | influxdb的地址 http://需要修改:8086/write?db=jmeter          | 是   |
| influxdbToken         | influxdb2的token令牌                                         | 是   |
| measurement           | influxdb的库名称，默认jmeter                                 | 是   |

# 11.[配置元件](https://jmeter.apache.org/usermanual/component_reference.html#config_elements)

配置元件，可以用于设置默认值和变量，供取样器使用。配置元件会在取样器之前被执行，作用域它和之后的地方。

## 11.1 CSV数据文件设置

CSV数据文件设置功能，用于从文件中读取行数据，并将它们拆分为变量。它比\_\_CSVRead() 和 \_\_StringFromFile() 函数都好用。它非常适合处理大量数据。

在运行时生成随机值，这种方式，CPU和内存的使用量是比较大的。使用一个文件中设置多个值，然后读取文件中内容，在运行时需要的cpu和内存要小很多。

JMeter允许引用的值带有引号。如果启动，则可以用双引号包裹值，然后，在使用时，自动去掉双引号。

JMeter还支持带有列标题的csv文件，可以配置是否启用“忽略首行”。

默认情况下，只会打开一次文件。并且每个线程将共享和使用文件的不同行，但是，行的取值顺序取决于取样器的执行顺序。每次迭代开始就从头开始取值。

如果期望每个线程都使用一组不同的值，可以创建多个不同的csv文件，文件名称前缀相同，后缀用不同的序号，在csv元件的文件名中，使用变量引用文件名称，然后，csv的共享模式设置为“当前线程”，这样，就可以实现每个线程使用一组不同的值。

![JMeter-11.1-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-11.1-1.jpg)

分隔符，根据文件中列与列的分隔符来确定。用‘\t’，表示制表符。

文件结尾是"<EOF>"， 如果“遇到文件结束符再次循环？”为true时，又会从第一行开始读取。如果为false，且“遇到文件结束符停止线程？”为false，则到了文末之后，所有的变量值都是"<EOF>", 这个值也可以通过“csvdataset.eofsting”属性来修改。如果“遇到文件结束符再次循环？”为false，而“遇到文件结束符停止线程？”为true，则到了文末，就会导致线程停止。

| 字段名                   | 描述                                                         | 必填 |
| ------------------------ | ------------------------------------------------------------ | ---- |
| 名称                     | 显示在脚本树中的元件名称                                     | 否   |
| 文件名                   | 要读取的文件名称。相对路径时，相对测试计划脚本路径进行解析。对于分布式测试，csv文件需要放在主控机器上。 | 是   |
| 文件编码                 | 读取文件的编码                                               | 否   |
| 变量名称                 | 变量名称列表，用逗号分隔变量名。                             | 否   |
| 忽略首行                 | 忽略第一行，当第一行为表头时，可以设置为true，忽略表头       | 是   |
| 分隔符                   | 拆分文件中列与列的分隔符号。                                 | 是   |
| 是否允许带有引号？       | 文件中的值，是否允许用双引号包裹。可以通过双引号把多个数据当作一个值。 | 是   |
| 遇到文件结束符再循环？   | 读取到文件末尾时，是否从头开始读取文件                       | 是   |
| 遇到文件结束符停止线程？ | 遇到文件末尾时，是否停止运行                                 | 是   |
| 线程共享模式             | 所有线程： 默认，文件在所有线程之间共享。<br />当前线程组： 每个线程组都打开一次文件，读取数据。<br />当前线程：每个线程都将打开一次文件。<br />编辑：相同标识符的所有线程共享同一个文件。 | 是   |

## 11.2 HTTP授权管理器

授权管理器允许为网页指定一个或多个用户登录名。

![JMeter-11.2-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-11.2-1.jpg)

| 字段名                   | 描述                                                 | 必填 |
| ------------------------ | ---------------------------------------------------- | ---- |
| 名称                     | 显示在脚本树中的元件名称                             | 否   |
| 每次迭代时清除身份验证？ | 如果选中，将在主线程组循环的每次迭代中完成身份验证。 | 是   |
| 基础URL                  | 与一个或多个HTTP请求地址匹配的部分或完整URL。        | 是   |
| 用户名                   | 要授权的用户名                                       | 是   |
| 密码                     | 用户密码                                             | 是   |
| 域                       | NTLM的域                                             | 否   |
| Realm                    | NTLM的领域                                           | 否   |
| Mechanism                | 要执行的身份验证的类型。                             | 否   |

## 11.3 HTTP缓存管理器

HTTP 缓存管理器用于向其范围内的 HTTP 请求添加缓存功能，以模拟浏览器缓存功能。 每个虚拟用户线程都有自己的缓存。默认情况下，缓存管理器将使用 LRU 算法在每个虚拟用户线程的缓存中存储最多 5000 个项目。

![JMeter-11.3-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-11.3-1.jpg)

| 字段名                                | 描述                             | 必填 |
| ------------------------------------- | -------------------------------- | ---- |
| 名称                                  | 显示在脚本树中的元件名称         | 否   |
| 在每次迭代中清除缓存？                | 选中，则在线程开始的时候清除缓存 | 是   |
| 处理GET请求时使用缓存控制\过期 消息头 | 更新GET方法的消息头              | 是   |
| 缓存中元素的最大数量                  | 5000                             | 是   |

## 11.4 HTTP cookie管理器

cookie管理器有两个功能：

+ 像浏览器一样存储和发送cookie。Cookie 管理器会自动存储该 cookie 并将 将其用于将来对该特定网站的所有请求
+ 手动将cookie添加到cookie管理器，cookie将由所有JMeter线程共享。

![JMeter-11.4-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-11.4-1.jpg)

| 字段名                | 描述                                                         | 必填 |
| --------------------- | ------------------------------------------------------------ | ---- |
| 名称                  | 显示在脚本树中的元件名称                                     | 否   |
| 每次反复清除cookies？ | 如果选中此选项，则每次执行主线程组循环时都会清除所有服务器定义的 Cookie。GUI中定义的cookie不会被清除。 | 是   |
| cookie策略            | cookie的策略                                                 | 是   |



## 11.5 HTTP请求默认值

设置 HTTP 请求控制器使用的默认值。如果你创建了多个取样器，并请求到同一个服务器，此时，可以添加HTTP请求默认值，配置“服务器名称或IP”，这样，http取样器可以继承默认值配置值

![JMeter-11.5-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-11.5-1.jpg)

| 字段名         | 描述                                  | 必填 |
| -------------- | ------------------------------------- | ---- |
| 名字           | 显示在脚本树中元件名称                | 否   |
| 协议           | 选择HTTP协议                          | 否   |
| 服务器名称或IP | 服务器的域名或IP地址，不要填写HTTP:// | 否   |
| 端口号         | http的端口                            | 否   |
| 路径           | 请求URL                               | 否   |
| 内容编码       | 请求体的编码                          | 否   |



## 11.6 HTTP消息头管理器

消息头管理器，配置取样器的消息头。

![JMeter-11.6-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-11.6-1.jpg)

## 11.7 JDBC 连接配置

配置数据库连接公共参数，池化数据库连接。

![JMeter-11.7-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-11.7-1.jpg)

| 字段名                                    | 描述                                                         | 必填 |
| ----------------------------------------- | ------------------------------------------------------------ | ---- |
| 名字                                      | 在脚本树中显示的元件名称                                     | 否   |
| Variable Name for created pool            | 绑定的连接池名称。                                           | 是   |
| Max Number of Connections                 | 连接池中允许的最大连接数。默认0，每个线程都能获得自己的池，连接之间不共享。 | 是   |
| Max Wait (ms)                             | 最长等待时间。如果超时，就会引发错误。                       | 是   |
| Time Between Eviction Runs (ms)           | 每次运行的时间间隔。空闲线程之间的休眠时长。                 | 是   |
| Auto Commit                               | 打开或关闭自动提交                                           | 是   |
| Transaction isolation                     | 事务隔离级别                                                 | 是   |
| Pool Prepared Statements                  | 每个连接池池化的最大准备语句数。-1 禁用池化，0 标识要池化的预准备语句数量不受限。 | 是   |
| Preinit Pool                              | 是否预初始化连接池。                                         | 否   |
| Init SQL statements separated by new line | 用换行符，分隔初始化语句。                                   | 否   |
| Test While Idle                           | 测试连接池的空闲连接。                                       | 是   |
| Soft Min Evictable Idle Time(ms)          | 在空闲对象被 ewareware 逐出之前，连接在池中可能处于空闲状态的最短时间，但附加条件是池中至少保留 **minIdle** 连接 | 是   |
| Validation Query                          | 验证查询，用于确定数据库是否仍在响应的简单查询               | 否   |
| Database URL                              | 数据库的JDBC连接串                                           | 是   |
| JDBC Driver class                         | 数据库连接驱动类， 启动jar包，需要放到JMeter的lib目录中      | 是   |
| Username                                  | 连接数据库的用户名                                           | 否   |
| Password                                  | 连接数据库的密码                                             | 否   |
| Connection Properties                     | 建立连接时要设置的连接属性                                   | 否   |

不同数据库的JDBC驱动程序需要不同配置。数据库URL和JDBC连接驱动类，由JDBC的jar文件中的实现定义。

| 数据库                                | 驱动类                                       | 数据库连接串                                                 |
| ------------------------------------- | -------------------------------------------- | ------------------------------------------------------------ |
| MySQL                                 | com.mysql.jdbc.Driver                        | jdbc:mysql://host[:port]/dbname                              |
| PostgreSQL                            | org.postgresql.Driver                        | jdbc:postgresql:{dbname}                                     |
| Oracle                                | oracle.jdbc.OracleDriver                     | jdbc:oracle:thin:@//host:port/service <br />或者 jdbc:oracle:thin:@(description=(address=(host={mc-name})(protocol=tcp)(port={port-no}))(connect_data=(sid={sid}))) |
| Ingress (2006)                        | ingres.jdbc.IngresDriver                     | jdbc:ingres://host:port/db[;attr=value]                      |
| Microsoft SQL Server (MS JDBC driver) | com.microsoft.sqlserver.jdbc.SQLServerDriver | jdbc:sqlserver://host:port;DatabaseName=dbname               |
| Apache Derby                          | org.apache.derby.jdbc.ClientDriver           | jdbc:derby://server[:port]/databaseName[;<br />URLAttributes=value[;…]] |
| MariaDB                               | org.mariadb.jdbc.Driver                      | jdbc:mariadb://host[:port]/dbname[;<br />URLAttributes=value[;…]] |
| Exasol                                | com.exasol.jdbc.EXADriver                    | jdbc:exa:host[:port][;schema=SCHEMA_NAME][;prop_x=value_x]   |



## 11.8 TCP取样器配置

TCP取样器配置TCP取样器的默认数据

![JMeter-11.8-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-11.8-1.jpg)

| 字段名              | 描述                                     | 必填 |
| ------------------- | ---------------------------------------- | ---- |
| 名字                | 在脚本树中显示的元件名称                 | 否   |
| TCPClient classname | TCPClient类名称。默认为tcp.handler的值。 | 否   |
| 服务器名称或IP      | TCP服务器的名称或IP                      | 是   |
| 端口号              | TCP服务的端口                            | 否   |
| 连接超时            | 连接超时，单位毫秒                       | 否   |
| 响应超时            | 响应超时，单位毫秒                       | 否   |
| Re-use connection   | 自动重连                                 | 是   |
| 关闭连接            | 如果选中，运行之后会关闭                 | 是   |
| 设置无延迟          | 设置无延迟                               | 是   |
| SO_LINGER           | 启用\|禁用SO_LINGER                      | 否   |
| 行尾 （EOL） 字节值 | 行尾的字节值，访问是[-128, 127]          | 否   |



## 11.9 用户自定义变量

用户定义的变量，与测试计划中的一样，可以定义一组初始变量。用户定义的变量，只会在第一次调用的时候，把结果保存到变量中，其他时候是不会保存变量的。

为简单起见，建议将用户自动定义的变量，放在线程组的开头。如果运行过程中使用用户参数或后置处理器定义了和用户定义的变量同名的变量，运行过程中，将会替换变量的初始值。



## 11.10 计数器

在线程组中任何地方，都可以添加计数器元件。计数器元件，可以配置起始值、最大值和增量。计数器从起始值开始到最大值，然后又从起始值开始循环，直到测试结束。

![JMeter-11.9-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-11.9-1.jpg)

| 字段名                       | 描述                                                         | 必填 |
| ---------------------------- | ------------------------------------------------------------ | ---- |
| 名字                         | 显示在脚本树中的元件名称                                     | 否   |
| Starting value               | 计数器起始值。默认为0                                        | 否   |
| 递增                         | 每次递增的步长                                               | 是   |
| Maximum value                | 最大值。如果超过最大值，就从起始值开始循环                   | 否   |
| 数字格式                     | 输出的数字格式，如000，输出的数值将为001,002...              | 否   |
| 引用名称                     | 计数器的变量名                                               | 否   |
| 与每用户独立的跟踪计数器     | 未选中，则该计数器是全局的，所有线程共用一个计数器。选中，则每个线程都独立一个计数器 | 否   |
| 在每个线程组迭代上重置计数器 | 如果选中，计数器将在每次线程组迭代时从起始值开始取值。       | 否   |

# 12.[前置处理器](https://jmeter.apache.org/usermanual/component_reference.html#preprocessors)

## 12.1 用户参数

可以为每个线程定义变量。用户参数，可以给一个变量定义多个值。在多线程时，会分别使用这些值。

![JMeter-12.1-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-12.1-1.jpg)

## 12.2 JSR223预处理器

JSR223预处理器，在取样器执行之前，先执行JSR223的脚本代码。

![JMeter-12.2-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-12.2-1.jpg)

| 字段名       | 描述                                                         | 必填 |
| ------------ | ------------------------------------------------------------ | ---- |
| 名字         | 显示在脚本树中元件的名称                                     | 否   |
| 语言         | JSR223脚本语言解释器                                         | 是   |
| 参数         | 传递给脚本的参数<br />parameters  单个变量<br />args 多个变量列表，用空格分隔 | 否   |
| 文件名       | 要运行的脚本文件                                             | 是   |
| 缓存编译脚本 | 是否缓存脚本语言。groovy语言支持。java\beanshell\javascript不支持。缓存将使用MD5hash。 | 否   |
| 脚本         | 要运行的脚本                                                 | 是   |

JSR223中可以引用的脚本方法：

+ log  用于写日志
+ Label 字符串标签
+ FileName 脚本文件名
+ Parameters 参数字符串
+ args 脚本字符串列表，用空格拆分
+ ctx 上下文访问
+ vars 变量操作  vars.get()   vars.put()
+ props  属性操作 props.get()  props.put()
+ sampler 取样器的访问
+ OUT 标准输出   OUT.println("输出的内容")



# 13.[后置处理器](https://jmeter.apache.org/usermanual/component_reference.html#postprocessors)

在取样器执行之后被执行。后置处理器的使用，要特别注意它的作用域。

后置处理器，在断言元件之前执行。

## 13.1 正则表达式提取器

允许使用Perl类型的正则表达式，从取样器的响应中提取想要的信息。正则提取器，使用正则表达式提取值，生成模板字符串，并将结果存储到用户参数中。

![JMeter-13.1-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-13.1-1.jpg)

| 字段名           | 描述                                                         | 必填 |
| ---------------- | ------------------------------------------------------------ | ---- |
| 名字             | 显示在脚本树中元件的名称                                     | 否   |
| Apply to         | 使用范围<br />+ 主和子取样器<br />+ 仅主取样器<br />+ 仅子取样器<br />+ 从JMeter变量中提取 | 是   |
| 要检查的响应字段 | 要检查的字段<br />+ 主体      响应到正文 <br />+ 正文(未转义)     响应的正文，替换了HTML转义代码<br />+ 正文当作文档     通过ApacheTika从各种类型文档中提取文本<br />+ 响应消息头   <br />+ 请求消息头 <br />+ URL  <br />+ 响应代码<br />+ 响应信息 | 是   |
| 引用名称         | 自定义的接收结果的变量名                                     | 是   |
| 正则表达式       | 正则表达式，() 小括号包裹正则式                              | 是   |
| 模板             | 查找匹配的模板 $1$ 中间数值代表第几组                        | 是   |
| 匹配数字         | 0 代表随机                                                   | 否   |
| 默认值           | 正则式没有匹配到结果时，使用的值。                           | 否   |

如果匹配数设置为非负数，并且发生匹配，则变量设置如下：

- refName - 模板的值
- refName_gn，其中 **n**=**0**，**1**，**2** - 匹配的组
- refName_g - 正则表达式中的组数（不包括 **0**)

如果未发生匹配，则 **refName** 变量设置为默认值（除非不存在）。 此外，还会删除以下变量：

- refName_g0
- refName_g1
- refName_g

如果匹配编号设置为负数，则将处理采样器数据中的所有可能匹配项。 变量设置如下：

- refName_matchNr - 找到的匹配项数;可以为 **0**
- refName_n，其中 **n** = **1**、**2**、**3** 等 - 模板生成的字符串
- refName_n_gm，其中 **m**=**0**， **1**， **2** - 匹配 **n** 的组
- refName - 始终设置为默认值
- refName_gn - 未设置



## 13.2 JSR223后置处理器

JSR223后置处理器，在取样器执行之后执行JSR223的脚本代码。

![JMeter-13.2-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-13.2-1.jpg)

| 字段名       | 描述                                                         | 必填 |
| ------------ | ------------------------------------------------------------ | ---- |
| 名字         | 显示在脚本树中元件的名称                                     | 否   |
| 语言         | JSR223脚本语言解释器                                         | 是   |
| 参数         | 传递给脚本的参数<br />parameters  单个变量<br />args 多个变量列表，用空格分隔 | 否   |
| 文件名       | 要运行的脚本文件                                             | 是   |
| 缓存编译脚本 | 是否缓存脚本语言。groovy语言支持。java\beanshell\javascript不支持。缓存将使用MD5hash。 | 否   |
| 脚本         | 要运行的脚本                                                 | 是   |

JSR223中可以引用的脚本方法：

+ log  用于写日志
+ Label 字符串标签
+ FileName 脚本文件名
+ Parameters 参数字符串
+ args 脚本字符串列表，用空格拆分
+ ctx 上下文访问
+ vars 变量操作  vars.get()   vars.put()
+ props  属性操作 props.get()  props.put()
+ sampler 取样器的访问
+ OUT 标准输出   OUT.println("输出的内容")

## 13.3 JSON提取器

从响应体为json的数据中提取想要的信息。

![JMeter-13.3-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-13.3-1.jpg)

| 字段名                     | 描述                                                         | 必填 |
| -------------------------- | ------------------------------------------------------------ | ---- |
| 名字                       | 显示在脚本树中元件的名称                                     | 否   |
| Apply to                   | 使用于<br />+ 主和子取样器<br />+ 仅主取样器<br />+ 仅子取样器<br />+ 从JMeter变量名中提取 | 是   |
| Names of created variables | 自定义的变量名，多个之间用分号分隔                           | 是   |
| JSON Path expressions      | json-path表达式                                              | 是   |
| Match to                   | 匹配值<br />0 代表随机<br />小于0的数字   提取的所有结果<br />大于0的数字   提取对应的值 | 否   |
| Default Values             | 默认值，json表达式返回不了值时使用默认值， 多个之间用分号分隔 | 否   |
| Compute concatenation var  | 计算串联变量， 使用逗号，把多个结果串联起来，并存储到  变量名_ALL 的变量中 | 否   |



## 13.4 边界提取器

使用左右边界从响应中提取值。

![JMeter-13.4-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-13.4-1.jpg)

| 字段名           | 描述                                                         | 必填 |
| ---------------- | ------------------------------------------------------------ | ---- |
| 名字             | 显示在脚本树中元件的名称                                     | 否   |
| Apply to         | 使用于<br />+ 主和子取样器<br />+ 仅主取样器<br />+ 仅子取样器<br />+ 从JMeter变量名中提取 | 是   |
| 要检查的响应字段 | 要检查的字段<br />+ 主体      响应到正文 <br />+ 正文(未转义)     响应的正文，替换了HTML转义代码<br />+ 正文当作文档     通过ApacheTika从各种类型文档中提取文本<br />+ 响应消息头   <br />+ 请求消息头 <br />+ URL  <br />+ 响应代码<br />+ 响应信息 | 是   |
| 引用的变量       | 自定义的变量名，多个之间用分号分隔                           | 是   |
| 左边界           | 要查找的值的左边界                                           | 否   |
| 右边界           | 要查找的值的右边界                                           | 否   |
| 匹配数字         | 匹配的次数，0表示随机，正数，表示匹配第几个，负数表示所有    | 是   |
| 缺省值           | 如果不匹配，使用默认值。                                     | 否   |

# 14.测试计划与线程组

## 14.1测试计划

测试计划是测试的总体设置的位置。

测试计划中可以设置用户自定义变量，这个变量是静态的，可以在整个测试计划中被引用。

测试计划中，可以配置多个线程组是串行运行还是并行运行，默认是并行运行。

测试计划中，tearDown线程组会在所有的主线程执行完之后再运行。

测试计划中，可以添加一些类实现， 如果删除类实现，必须重启JMeter。类实现中不能添加JMeter的插件，插件会先运行。

## 14.2 线程组

线程组定义个用户池，在线程组 GUI 中，您可以控制模拟的用户数（线程数）、启动时间（启动所有线程所需的时间）、执行测试的次数，以及测试的开始和停止时间。

![JMeter-14.2-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-14.2-1.jpg)

| 字段名                      | 描述                                                         | 必填 |
| --------------------------- | ------------------------------------------------------------ | ---- |
| 名字                        | 显示在脚本树中元件的名字                                     | 否   |
| 在取样器错误后要执行的动作  | + 继续   忽略错误并继续执行测试<br />+ 启动下一进程循环    忽略错误，启动下一个循环并继续测试<br />+ 停止线程     退出当前线程<br />+ 停止测试    停止当前测试<br />+ 立即停止测试   立刻停止当前的测试 | 是   |
| 线程数                      | 要模拟的并发用户数                                           | 是   |
| Ramp-up时间                 | JMeter启动所有线程的时间                                     | 是   |
| 循环次数                    | 执行测试的迭代次数，可以勾选‘永远’，直到手动停止或达到执行的生存周期结束 | 是   |
| Same user on each iteration | 每次迭代时，使用相同用户信息。如果选中，则后续所有请求的cookie和缓存数据都来自第一个取样器的响应，未选中，就相反。 | 是   |
| 延迟创建线程直到需要        | 选中，严格按启动时间来启动线程。未选中，会在开始时准备所有线程。 | 是   |
| 调度器                      | 如果选中，则线程执行按给定的时间范围执行                     | 是   |
| 持续时间 秒                 | 选中调度器，可以编辑此时间。JMeter将按这个时间来执行         | 否   |
| 启动延迟 秒                 | 选中调度器，可以编辑此时间。JMeter将按这个时间来延迟启动线程组运行。 | 否   |

# 15.[JMeter属性配置](https://jmeter.apache.org/usermanual/properties_reference.html#introduction)

JMeter属性，主要是 jmeter.properties 和 reportgenerator.properties 文件中配置的属性信息。修改这些属性配置，应该写到user.properties文件中。修改属性配置之后，要重启JMeter。

## 15.1 JMeter语言属性

+ language   JMeter的GUI界面语言，默认注释掉了，使用缺省配置en，英文。这个属性，是唯一一个必须在jmeter.properties文件中修改的，不能再user.properties文件中配置。
+ language.add   该属性中显示了JMeter支持的所有语言。 zh_CN 是简体中文。

## 15.2 SSL属性

系统ssl属性，现在位于system.properties文件中，JMeter不再在 javax.xxxx 属性写到 jmeter.properties 文件中。系统属性，必须在system.properties文件中定义。

+ https.sessioncontext.shared		默认为false， 在ssl会话上，不共享上下文
+ https.default.protocol           默认TLS， https的默认加密认证方式，可以修改为 SSLv3
+ https.socket.protocols          启用的协议列表
+ https.cipherSuites                 可以在HTTPS中使用的SSL密码套件，用逗号分隔。
+ httpclient.reset_state_on_thread_group_iteration         在启动新线程组是重置HTTP状态。默认 true
+ httpclient.reset_state_on_thread_group_iteration       在迭代之间重置SSL上下文缓存
+ https.keyStoreStartIndex       启用索引，默认0
+ https.keyStoreEndIndex      约束索引，默认0

## 15.3 GUI外观配置

+ jmeter.laf.windows_10     WIN10系统UI
+ jmeter.loggerpanel.display     显示logger面板，默认false
+ jmeter.loggerpanel.maxlength     logger面板的最大行数，默认1000， 0表示不限制
+ jmeter.gui.refresh_period       GUI界面刷新频率，单位毫秒， 默认500

## 15.4 GUI工具栏配置

+ jmeter.toolbar.icons       工具栏图标
+ jmeter.toolbar              工具栏列表
+ jmeter.toolbar.icons.size      工具栏图标大小
+ jmeter.icons                JMeter图标
+ jmeter.tree.icons.size    JMeter树图标大小
+ jmeter.hidpi.mode    JMeter的HiDPI模式
+ jmeter.hidpi.scale.factor      HiDPI比例因子
+ not_in_menu        不显示的菜单
+ gui.quick_X        GUI的快捷键配置

## 15.5 JMX备份配置

+ jmeter.gui.action.save.backup_on_save      在GUI界面中，启动自动保存jmx文件功能
+ jmeter.gui.action.save.backup_directory      在GUI界面中自动保存jmx文件的文件夹
+ jmeter.gui.action.save.keep_backup_max_hours        设置备份保留的时长，默认没有设置，永久保留。
+ jmeter.gui.action.save.keep_backup_max_count        设置最大保留份数，默认保留10份。
+ save_automatically_before_run       在运行测试计划之前，保存

## 15.6 远程主机和RMI配置

+ remote_hosts     远程主机，多个之间用逗号分隔
+ server_port     认证服务端口
+ client.rmi.localport       使用的 RMI 端口的参数 默认值为 0
+ client.try     尝试初始化远程引擎的次数，默认 1
+ client.retries_delay      尝试重新初始化远程引擎的时间间隔
+ client.continue_on_fail      初始化重连失败处理
+ server.rmi.port       分布式服务端口，默认1099
+ server.rmi.create    启动时创建RMI认证
+ server.exitaftertest       测试退出之后
+ server.rmi.ssl.keystore.type     用于RMI连接的密钥类型，默认jks
+ server.rmi.ssl.keystore.file        密钥文件  默认 rmi_keystore.jks
+ server.rmi.ssl.keystore.password     密钥的密码，默认 changeit
+ server.rmi.ssl.keystore.alias     密钥的别名,  默认 RMI
+ server.rmi.ssl.truststore.type    受信任的密钥类型   JKS
+ server.rmi.ssl.truststore.file      受信任的秘密文件 默认  rmi_keystore.jks
+ server.rmi.ssl.truststore.password   受信任的密钥存储到密码  默认 changeit
+ server.rmi.ssl.disable        不启动加密认证传输，默认 flase， 大多时候要设置为true

## 15.7 Apache HTTPClient 通用属性

+ http.post_add_content_type_if_missing       如果没有消息头，会自动添加一个消息头，在JMeter4.1版本以前是自动添加 Content-Type: application/x-www-form-urlencoded  现在默认为false
+ httpclient.timeout         http连接超时时长， 0表示无超时，默认为0
+ httpclient.version       设置http版本，默认1.1

+ httpclient.socket.http.cps      将http每秒字符数设置为大于零的值以模拟慢速连接。默认值 0
+ httpclient.socket.https.cps     将https每秒字符数设置为大于零的值以模拟慢速连接。默认值 0
+ httpclient.loopback      启用循环协议 ，默认 true
+ httpclient.localaddress     主控机的ip
+ http.proxyUser        http代理的用户名
+ http.proxyPass        http代理的密码

## 15.8 HTTPClient4 属性配置

+ hc.parameters.file         覆盖 Apache HttpClient 参数的属性文件， 默认 hc.parameters
+ httpclient4.auth.preemptive      使用基本身份验证时抢先发送授权标头  默认 true
+ httpclient4.retrycount      尝试的重试次数 ，默认0
+ httpclient4.request_sent_retry_enabled      重试幂等和非幂等请求， 默认flase
+ httpclient4.idletimeout     空闲连接超时，单位 毫秒，默认 0
+ httpclient4.validate_after_inactivity      检查从上次连接之后的时长， 默认4900
+ httpclient4.time_to_live    TTL的时长，单位毫秒， 默认60000
+ httpclient4.deflate_relax_mode       忽略 EOFException 信号，默认 false
+ httpclient4.gzip_relax_mode        忽略EOFException信号指定的GZiped， 默认false
+ httpclient4.default_user_agent_disabled     是否默认ua头，默认 false

## 15.9 HTTP 缓存管理器配置

+ cacheable_methods      可以缓存的方法的空格或逗号分隔列表，默认GET
+ cache_manager.cached_resource_mode      缓存资源的模式， 默认RETURN_NO_SAMPLE
+ RETURN_CUSTOM_STATUS.code    RETURN_CUSTOM_STATUS时的返回码，默认空
+ RETURN_CUSTOM_STATUS.message      RETURN_CUSTOM_STATUS时的消息，默认空

## 15.10 结果文件配置

+ jmeter.save.saveservice.output_format    JMeter保存文件的格式，支持xml,csv,db，默认csv
+ jmeter.save.saveservice.assertion_results_failure_message       是否保存断言失败信息到csv文件
+ jmeter.save.saveservice.assertion_results      断言结果，有效值none,first,all。 默认 none
+ jmeter.save.saveservice.data_type       是否保存 data_type ，默认true
+ jmeter.save.saveservice.label       是否保存label，默认true
+ jmeter.save.saveservice.response_code     是否保存响应码， 默认true
+ jmeter.save.saveservice.response_data     是否保存响应体，csv格式不支持，默认false
+ jmeter.save.saveservice.response_data.on_error     是否保存失败响应体，默认fasle
+ jmeter.save.saveservice.response_message      是否保存response_message，默认true
+ jmeter.save.saveservice.successful     是否保存成功信息，默认true
+ jmeter.save.saveservice.thread_name    是否保存线程名称， 默认ture
+ jmeter.save.saveservice.time     是否保存时间， 默认true
+ jmeter.save.saveservice.subresults   是否保存子取样器结果， 默认true
+ jmeter.save.saveservice.assertions     是否保存断言结果， 默认true
+ jmeter.save.saveservice.latency     是否保存latency时长，默认true
+ jmeter.save.saveservice.connect_time    是否保存connect_time时长， 默认false
+ jmeter.save.saveservice.samplerData    是否保存样本数据，默认false
+ jmeter.save.saveservice.responseHeaders     是否保存响应头， 默认false
+ jmeter.save.saveservice.requestHeaders       是否保存请求头， 默认false
+ jmeter.save.saveservice.encoding      是否保存编码， 默认false
+ jmeter.save.saveservice.bytes       是否保存字节大小， 默认true
+ jmeter.save.saveservice.url       是否保存URL地址， 默认false
+ jmeter.save.saveservice.filename     是否保存文件名，默认 false
+ jmeter.save.saveservice.hostname    是否保存主机名， 默认false
+ jmeter.save.saveservice.thread_counts   是否保存线程数量， 默认true
+ jmeter.save.saveservice.sample_count      是否保存样本数量， 默认false
+ jmeter.save.saveservice.idle_time        是否保存空闲时间 ， 默认true
+ jmeter.save.saveservice.timestamp_format       时间格式，默认ms
+ jmeter.save.saveservice.default_delimiter   csv格式的分隔符，默认 逗号
+ jmeter.save.saveservice.print_field_names      是否输出表头，只在csv格式时有效，默认true
+ jmeter.save.saveservice.base_prefix    文件的相对地址
+ jmeter.save.saveservice.autoflush    写入结果时，自动刷新，默认false
+ sampleresult.timestamp.start        保存开始时间戳， 默认false
+ sampleresult.useNanoTime     是否使用System.nanoTime()， 默认true
+ sampleresult.nanoThreadSleep     用一个后台进程计算时间偏移，默认5000

## 15.11 分布式请求结果处理模式配置

从JMeter2.9版本开始，在用分布式进行性能测试时，测试数据，默认mode模式是分批从服务器上返回的，这个mode模式可以修改，可选的模式有：

+ Standard   逐个返回样本结果
+ Batch      累积结果，分批返回
+ Statistical     只返回样本汇总统计概括信息
+ Stripped     与Standard模式类似，但是把响应从样本结果中剥离出来。
+ StrippedBatch   与Batch类似，但是，把取样结果删除了
+ Asynch  异步发送，
+ StrippedAsynch   与Asynch类似，但是从取样结果中去除了响应数据。
+ StrippedDiskStore  与DiskStore类似，但是，从取样结果中去除了响应数据

## 15.12 JDBC请求属性配置

+ jdbcsampler.nullmarker      空字符串，默认 "]NULL["
+ jdbcsampler.max_retain_result_size      JDBC保存的最大结果大小
+ jdbc.config.check.query        用于确定数据库是否仍在响应的查询列表
+ jdbc.config.jdbc.driver.class      JDBC的驱动类名列表

## 15.13 TCP取样器的属性配置

+ tcp.handler      默认 TCPClientImpl
+ tcp.eolByte      跳过eol检查的范围[-128, 127]
+ tcp.charset       默认字符集
+ tcp.status.prefix     状态码前缀
+ tcp.status.suffix      状态码后缀
+ tcp.status.properties      状态转换为属性的文件
+ tcp.binarylength.prefix.length     二进制文件前缀长度

## 15.14 CLI模式的聚合报告

+ summariser.name      聚合报告的名称
+ summariser.interval     聚合报告的时间间隔
+ summariser.log     聚合报告日志
+ summariser.out     聚合输出
+ summariser.ignore_transaction_controller_sample_result    忽略事务控制器生成的结果

## 15.15 后端监听器配置

+ backend_graphite.send_interval         发送给graphite的时间间隔，单位秒， 默认1秒
+ backend_influxdb.send_interval          发送给influxdb的时间间隔，单位秒，默认5秒
+ backend_influxdb.connection_timeout     连接influxdb的超时时长，单位毫秒， 默认1000毫秒
+ backend_influxdb.socket_timeout         influxdb的套接字超时读取时长，单位毫秒，默认3000毫秒
+ backend_influxdb.connection_request_timeout      influxdb超时请求时长，单位毫秒， 默认100毫秒

## 15.16 其他属性配置

+ resultcollector.action_if_file_exists    保存结果到文件时，如果文件存在，怎么处理。有三种：①ASK询问方式；②APPEND追加到文件末尾；③删除现有文件，新建文件。
+ jmeter.expertMode    打开\关闭专家模式
+ httpsampler.max_bytes_to_store_per_request     每个取样器结果存储在内存中的最大字节大小。
+ httpsampler.max_buffer_size     HTTP取样器的最大读缓存大小，默认66560byte
+ httpsampler.max_redirects     单个http取样器的最大重定向数量，默认20
+ httpsampler.max_frame_depth       单个http取样器的最大帧深度，默认5
+ httpsampler.ignore_failed_embedded_resources     忽略资源下载错误
+ httpsampler.parallel_download_thread_keepalive_inseconds    保持并行下载线程的活跃时间，默认60秒
+ httpsampler.embedded_resources_use_md5      不要保留嵌入的资源响应数据
+ httpsampler.user_defined_methods     自定义的其他HTTP方法
+ sampleresult.default.encoding      取样器的结果编码，默认ISO-8859-1
+ CookieManager.delete_null_cookies     是否删除空cookie， 默认 true
+ CookieManager.allow_variable_cookies    是否允许改变cookie， 默认 true
+ CookieManager.save.cookies    是否把cookie存储为变量， 默认 false
+ jmeterengine.threadstop.wait     等待线程停止时长， 单位 毫秒，默认5000
+ jmeterengine.remote.system.exit     退出时是否停止RMI， 默认 false
+ jmeterengine.stopfail.system.exit     在CLI模式时，停止线程是否调用System.exit(1) 退出， 默认 true
+ jmeterengine.force.system.exit     在CLI模式时，结束测试是否调用System.exit(0)退出， 默认false
+ jmeterengine.nongui.port      CLI模式，默认监听消息的端口，默认 4445
+ jmeterengine.nongui.maxport    CLI模式，监听消息的最大端口，默认4455
+ view.results.tree.max_results      查看结果树，最大显示数量，默认 500
+ view.results.tree.max_size       查看结果树最大大小， 默认 10485760
+ view.results.tree.max_line_size    查看结果树行的最大大小  默认 110000byte
+ jmeter.reportgenerator.apdex_satisfied_threshold     报告中APDEX的容忍值 单位毫秒，默认500
+ jmeter.reportgenerator.apdex_tolerated_threshold     报告中APDEX的最大可接受极限时长，单位毫秒，默认1500
+ jmeter.reportgenerator.sample_filter   报告中，过滤器
+ jmeter.reportgenerator.temp_dir        报告的临时文件夹
+ jmeter.reportgenerator.report_title       报告的标题， 默认 Apache JMeter Dashboard
+ jmeter.reportgenerator.overall_granularity       报告的取点时间间隔， 默认60000

# 16.[函数](https://jmeter.apache.org/usermanual/functions.html)

JMeter核酸可以在任何的取样器和元件中被使用。函数的使用如： ${\_\_函数名称(参数1，参数2,...)}。 括号和括号中的参数，因函数而异。不需要参数的函数，可以省略括号。参数用逗号分隔，如果参数值中有逗号，需要转义。

函数使用错误，JMeter不会报告\记录错误。在JMeter中，变量名、函数名、属性名都是严格区分大小写的。

JMeter函数有内置函数和扩展函数。

## 16.1 函数对话框

可以在JMeter的菜单栏中，找到函数助手对话框，在里面，选择函数

![JMeter-16.1-1.jpg](E:\gitee\TranslateJmeterUserGuidTozh\images\JMeter-16.1-1.jpg)

使用函数助手， 你可以在下拉列表中选择一个函数， 函数参数列表会显示函数的参数，列表左侧是参数说明，右侧是函数参数值。不同的函数，参数不同。填写好参数，点击‘生成’按钮，会自动生成函数，并复制到粘贴板，可以在测试计划的任何地方粘贴使用。

## 16.2 计数器函数 \_\_counter

计数器函数，每次调用，自动计数。从1开始，每次递增1。在一次迭代中，有多个计数器函数，不会每次递增1.

+ 第一个参数     默认 false，为全局计数器，所有的都共用一个计数器。True，为每个用户单独计数器。
+ 第二个参数     引用此函数值的变量名

## 16.3 获取线程组函数 \_\_threadNum

该函数返回当前正在执行的线程编号。该函数，没有参数。

## 16.4 整数求和函数 \_\_intSum

intSum函数，可以用于两个或多个整数求和。

+ 第1个参数      必填，第一个整数
+ 第2个参数      必填，第二个整数
+ 第n个参数     可选，第n个整数
+ 最后一个参数      引用此函数值的变量名

## 16.5  从文本文件中读取字符串 \_\_StringFromFile

StringFromFile函数，是从文件中读取字符串。相比csv元件，它支持多个输入文件。每次调用它，它都会从文件中读取下一行。所有线程共享一个文件实例。当读取到文件末尾时，又会从头开始读取。

如果一个脚本中，有多次调用该函数，则会打开多个文件。如果打开或读取文件出错，返回“\*\*ERR\*\*”

+ 第1个参数       文件名路径，必填
+ 第2个参数       应用的变量名
+ 第3个参数       开始文件名序号
+ 第4个参数       结束文件名序号

## 16.6 \_\_javaScript 函数

javaScript函数，用来执行javascript脚本。

javaScript函数，使用独立的javascript解析器。 JavaScript不是JMeter中最佳语言脚本，如果有大量线程，建议用jexl3或groovy函数。

+ 第1个参数     javascript可以执行的脚本
+ 第2个参数    引用此函数的结果变量名

## 16.7 \_\_Random 随机函数

从一个最小值和最大值之间随机生成一个数

+ 第1个参数    最小值，必填
+ 第2个参数    最大值，必填
+ 第3个参数    引用此函数的值的变量

## 16.8 \_\_RandomData 随机日期函数

从给定的开始日期和结束日期之间随机一个日期

+ 第1个参数    时间格式
+ 第2个参数    开始日期
+ 第3个参数    结束日期，必填
+ 第4个参数    时间格式地区
+ 第5个参数    存储此函数值的变量名

## 16.9 \_\_RandomString 随机字符串函数

随机字符串函数，根据给定的长度返回一个随机字符串。

+ 第1个参数    字符串的长度
+ 第2个参数     用于生成字符串的字符
+ 第3个参数    存储此函数值的变量名

## 16.10 \_\_RandomFromMultipleVars 多变量随机取值

RandomFromMultipleVars函数是从对多个值中随机取值

+ 第1个参数    源变量，多个之间用 | 分隔
+ 第2个参数    存储此函数值的变量名

## 16.11 \_\_property 获取属性函数

获取JMeter的属性值，如果找不到属性值并且未提供默认值，则返回属性名称。

+ 第1个参数     属性名称
+ 第2个参数     存储此函数运行的结果的变量名
+ 第3个参数     默认值

## 16.12 \_\_P  获取属性函数

获取JMeter属性函数，与上一个相同，是简化版。

## 16.13 \_\_groovy groovy函数

执行groovy脚本的函数。

+ 第1个参数     groovy脚本
+ 第2个参数    存储此函数运行结果的变量名

## 16.14 \_\_split 分隔函数

splite函数，根据提供的分隔符分隔给定的字符串。

+ 第1个参数   要分隔的字符串
+ 第2个参数   存储此函数的结果的变量名
+ 第3个参数    分隔符号

## 16.15 \_\_setProperty 设置属性函数

设置JMeter属性函数。

+ 第1个参数     属性名称
+ 第2个参数     属性的值
+ 第3个参数     是否返回原值

## 16.16 \_\_time 获取当前时间戳函数

获取当前的时间戳

+ 第1个参数     时间格式。 有YMD   HMS  YMDHMS 几种固定格式，也可以自定义格式
+ 第2个参数   存储此函数的值的变量名

## 16.17 \_\_jexl3 运行jexl脚本函数

jexl3运行jexl表达式结果函数

+ 第1个参数     jexl的表达式
+ 第2个参数     存储此函数运行结果的变量名

## 16.18 \_\_V 拼接函数

V函数是符合变量名称计数结果函数。

+ 第1个参数     变量名
+ 第2个参数     默认值

## 16.19 \_\_evalVar返回存储在变量中的表达式的结果

evalVar函数，返回存储在变量中表达式的计数结果

+ 第1个参数      要计数的变量

## 16.20 \_\_eval 运行字符串表达式结果

运行字符串表达式的结果

+ 第1个参数     要计算的字符串表达式

## 16.21 \_\_char 数字转字符串函数

数字转unicode字符

+ 第1个参数    要转unicode的数字

## 16.22 \_\_unescape java转义字符的结果

把转义内容，转换为字符串

+ 第1个参数     要转义的内容

## 16.23 \_\_urldecode 解码url编码字符串

解码已经被url编码的字符串

+ 第1个参数    要解码的字符串

## 16.24 \_\_urlencode 编码url字符串

编码要进行url编码的字符串

+ 第1个参    要编码的字符串

## 16.25 \_\_FileToString读取文件

FileToString读取文件

+ 第1个参数     文件名
+ 第2个参数     文件编码
+ 第3个参数     存储此文件的变量名

## 16.26 \_\_timeShift 时间偏移转化

timeShift时间偏移函数

+ 第1个参数      时间格式
+ 第2个参数      起始日期
+ 第3个参数     偏移量。 以P开头，没有日期只有时间的话再加T，日期D，H小时，M分钟，S秒，“-”负数，代表减法，数字没有符号代表加法。
+ 第4个参数     时间区域
+ 第5个参数      存储此函数结果的变量名

## 16.27 \_\_digest 加密函数

加密函数

+ 第1个参数     加密算法，MD2  MD5 SHA-1    SHA-224   SHA-256   SHA-384   SHA-512
+ 第2个参数     被加密的内容
+ 第3个参数    要加盐的字符串
+ 第4个参数     加密结果大小写，true为大写
+ 第5个参数    存储此函数结果的变量名

## 16.28 \_\_dataTimeConvert 时间格式转化

dataTimeConvert时间格式转化函数

+ 第1个参数    日期字符串
+ 第2个参数     源日期格式
+ 第3个参数     新的日期格式
+ 第4个参数      存储此函数结果的变量名

## 16.29 \_\_StringToFile 字符串写入文件

StringToFile函数，将字符串写入文件。

+ 第1个参数       文件路径
+ 第2个参数       要写入到字符串
+ 第3个参数       写文件的方式， true为追加，false为覆盖
+ 第4个参数      写入文件的字符串编码，默认utf-8

# 17.[分布式](https://jmeter.apache.org/usermanual/remote-test.html)

如果你的JMeter客户端机器无法模拟足够数量的用户，用于向被测服务器发起请求压力，或存在网络限制，这个时候，就可以使用JMeter控制多个JMeter客户端引擎的方式来实现。通过远程控制运行JMeter，你可以利用多个低配的电脑来发起请求，从而实现模拟大量的用户对服务器造成很大请求压力。JMeter可以控制任意多数量的JMeter实例，并收集它们的测试数据。

JMeter分布式：

+ 将所有机器的测试结果保存在主控机上
+ 主控机器可以管理多个JMeterEngines
+ 无需将测试计划复制到每个分布式机器上
+ 所有的分布式机器都运行相同的测试计划，JMeter不会在分布式机器上分配负载，每个分布式机器都运行完整、一样的测试计划，所以，每个分布式机器运行的线程数都是相等的。

使用相同数量的线程数进行性能测试，分布式要比CLI单机使用更多的资源。主控机器可能会出现过载或网络连接过载问题，这需要把主控机的模式配置为Stripped模式，但是这依然会有过载可能。

注意： 虽然助攻服务可以在被测服务器上部署，但是这会导致被测服务受到干扰，所以，建议助攻服务是单独的机器，你可以配置这些助攻机器的网络与服务器网络相同，这样可以最大限度减少对被测服务的影响。

**助攻服务配置：**

+ 所有的助攻机器上完全相同的JMeter版本
+ 所有的助攻机器上相同的java版本
+ 使用SSL有效密钥库，或禁用SSL

如果测试脚本中使用了数据文件，主控机器是不会发送这些文件到助攻机器上的，需要确保每个助攻服务上都能获取到文件。如果有必要，可以通过每台助攻服务的user.properties 或 system.properties 文件来配置文件路径属性。属性配置，会在服务启动时被加载。

**启动助攻服务：**

每台助攻服务器都需要启动服务，在uninx中可以使用 JMETER_HOME/bin/jmeter-server  在windows下使用 JMETER_HOME/bin/jmeter-server.bat。

注意： 除非使用不同的RMI端口，否则一台机器，只能启动一个JMeter助攻服务。

默认情况下，启动JMeter助攻服务的时候，端口是动态生成的，这可能导致防火墙失效，所以，可以配置server.rmi.localport属性，来指定端口。

**讲助攻服务的ip和端口添加到主控机器的属性配置中：**

在jmeter.properties属性配置文件中，找到“remote_hosts”属性，把JMeter助攻机器的ip和端口写在这里，多个之间用逗号分隔。

您也可以通过在命令行中使用 -R 参数，来指定要使用的助攻机器。这个与 -r 或 -Jremote_hosts={助攻服务器列表} 具有同等效果。

如果属性server.exitaftertest=true 为true，则在测试完成之后，助攻服务将退出服务。

**通过GUI界面检查助攻服务配置：**

启动JMeter的图形界面，在“运行”菜单下，有两个子菜单“远程启动” 和 “远程停止”

![JMeter-17.1-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-17.1-1.jpg)

通过这两个菜单，可以远程启动调用助攻服务或停止，而不是通过普通的 启动和停止 菜单。

**使用CLI模式调用助攻服务：**

GUI模式，仅用于调试，CLI模式用于调用助攻服务进行测试。

操作的命令： `jmeter -n -t script.jmx -r`   或者 `jmeter -n -t script.jmx -R sever1,server2...` 

参数，也可以使用 `-Gproperty=值`  在所有助攻服务中定义一个属性， 和 `-X` 在测试结束后，退出助攻服务。 

## 17.1 设置SSL

从JMeter4.0开始，RMI默认传输机制使用SSL,SSL需要密钥和证书才能工作。JMeter自带了一个生成密钥库和证书的脚本。该脚本在bin目录中，windows是create-rmi-keystroe.bat文件，在unix系统中是create-rmi-keystroe.sh文件。使用它，会生成一个有效期为7天的密钥对。

运行生成密钥的脚本，输入问题答案，即可生成密钥文件rmi_keystore.jks

![JMeter-17.1-2.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-17.1-2.jpg)

最后，回车。即可生成rmi_keystore.jks 文件，然后，把这个文件手动分发的所有的助攻服务的JMeter的bin目录中。

## 17.2 提示

JMeter进行分布式测试，主控机器和助攻机器之间的连接，默认用的是1099端口。主控机器上，还要开发高位端口，这些端口可以在jmeter.properties文件中client.rmi.localport属性配置。这个值，如果不是0，则就是高位端口的起始值。在分布式中，要特别注意防火墙上这些端口。如果你使用的suse linux，哪这个防火墙的提示信息就很有用了。当然，也可以开启调试模式。

## 17.3 使用其他端口

默认情况下，JMeter助攻服务使用1099端口，可以更改。

+ 修改助攻服务配中的 server_port端口启动
+ 在主控机器上，修改remote_hosts的配置为助攻机器ip:端口。

## 17.4 使用不同模式返回测试数据

JMeter测试计划的监听器会将结果返回给主控机器，写入指定的文件。默认情况下，是同步返回的。这会影响测试时的最大吞吐量，需要返回数据之后，才会继续测试。默认的模式时StrippedBatch

+ 模式：
  + Standard      实时返回测试数据
  + Hold       先将数据保存，结束时再返回给主控机
  + DiskStore     将数据临时存储在磁盘文件中，助攻服务停止时删除
  + StrippedDiskStore     在成功的样本中删除响应数据，把存储在磁盘上的数据，再返回。
  + Batch   根据属性num_sample_threshold(默认100) 和 time_threshold(默认60秒) 的配置值，分别返回数据。
  + Statistical    超过num_sample_threshold(默认100) 和 time_threshold(默认60秒) 的配置值时，返回概要信息。概要信息包括： elasped time、latency、bytes、sample count、error count
  + Stripped   从样本中删除响应数据
  + StrippedBatch    从样本中删除响应数据，然后分批返回数据
  + Asynch   异步返回
  + StrippedAsynch      从成功样本中删除响应数据， 然后异步返回数据
  + Custom implementation   自定义实现

## 17.5 处理异常的助攻节点

在使用多个助攻机器一起执行分布式测试时，可能会出现部分助攻服务不可用。

+ 首先，可以尝试重试连接。修改client.try属性配置，默认时值为1，还可以设置client.retries_delay属性值，设置重试连接之间的间隔时长。
+ 然后，可以配置 client.continue_on_fail=true，设置使用成功的助攻服务进行测试，跳过失败的助攻服务。

# 18.[测试报告](https://jmeter.apache.org/usermanual/generating-dashboard.html)

JMeter支持生成仪表板报告。

## 18.1概述

仪表板生气器，是JMeter的模块化扩展，默认是读取和处理csv文件数据，生成包含图表视图的HTML文件。

报告中，包括：

+ APDEX  用户满意度指数。 用表格方式展示根据可接受阈值和容忍极限阈值，计算的每个事务的满意度指数。
+ 显示成功和失败的百分比概要图
+ 一个概要统计图表，展示重要的指标数据
+ 一个错误图表，展示错误信息
+ 一个top5错误， 展示最高的5中错误
+ 响应时间图表
+ 一段时间的百分位数
+ 一段时间的活跃线程数
+ 一段时间的吞吐量
+ 一段时间的延迟时间
+ 一段时间的连接时间
+ 每秒点击率
+ 每秒响应代码数
+ 每秒事务数
+ 响应时间与每秒请求数
+ 延迟与每秒请求数
+ 响应时间概述
+ 响应时间百分位数
+ 时间与线程
+ 响应时间分布

## 18.2 仪表板配置参数

生成报告的所有配置都在reportgenerator.properties中。

+ 过滤器   jmeter.reportgenerator.exporter.html.series_filter 保留符合条件的事务数据，生成报告
+ 测试时，保存数的数据配置。如下配置是必须要有的

```properties
jmeter.save.saveservice.bytes = true
# Only available with HttpClient4
#jmeter.save.saveservice.sent_bytes=true
jmeter.save.saveservice.label = true
jmeter.save.saveservice.latency = true
jmeter.save.saveservice.response_code = true
jmeter.save.saveservice.response_message = true
jmeter.save.saveservice.successful = true
jmeter.save.saveservice.thread_counts = true
jmeter.save.saveservice.thread_name = true
jmeter.save.saveservice.time = true
jmeter.save.saveservice.connect_time = true
jmeter.save.saveservice.assertion_results_failure_message = true
# the timestamp format must include the time and should include the date.
# For example the default, which is milliseconds since the epoch:
jmeter.save.saveservice.timestamp_format = ms
# Or the following would also be suitable
# jmeter.save.saveservice.timestamp_format = yyyy/MM/dd HH:mm:ss
```

+ 事务控制器，不选中Generate parent sample； 如果把事务控制器当作一个容器来使用，可以右键事务控制器，然后选择应用命名策略。

报告文件相关配置参数，以jmeter.reportgenerator. 开头

+ report_title  报表的标题，默认 Apache JMeter Dashboard
+ data_format    日期格式，默认yyyyMMddHHmmss
+ start_data     报告的数据开始日期
+ end_data       数据的结束日期
+ overall_granularity    时间粒度，默认1分钟。 这个值必须大于1秒
+ apdex_statisfied_threshold   满意的阈值，默认500毫秒
+ apdex_tolerated_threshold   可接受的极限阈值，默认1500毫秒
+ jmeter.reportgenerator.apdex_per_transaction     为特定样本设定满意和极限阈值
+ sample_filter    样本过滤器
+ temp_dir    临时文件夹

## 18.3 生成报告

生成测试报告，可以是一个独立过程。

+ 使用现有csv文件生成报告     `jmeter -g  jtl文件 -o  报告文件夹名`

+ CLI 命令执行后生成报告 ` jmeter -n -t jmx脚本 -l jtl文件 -e -o  报告文件夹`

+ csv文件在图形界面中生成报告， 菜单栏   工具 > Generate HTML Report

  ![JMeter-18.1-1.jpg](E:/gitee/TranslateJmeterUserGuidTozh/images/JMeter-18.1-1.jpg)

  + Results file (csv or jtl)     使用的csv文件
  + user.properties file      属性配置文件
  + Output directory      结果输出到的文件夹，如果未定义输出目录，控制器将使用 bin/report-output

