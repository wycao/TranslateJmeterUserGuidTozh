# 06.[开发SOAP协议脚本](https://jmeter.apache.org/usermanual/build-ws-test-plan.html)

## 6.1 创建测试计划

在模板中选择 “构建SOAP服务计划”，然后“创建”

![JMeter-6.1-1.jpg](images/JMeter-6.1-1.jpg)

改变以下内容：

+ 在 HTTP请求默认值 中，更改 服务器名称或IP
+ 在 SOAP请求 中，更改 路径。

接下来，在 HTTP消息够管理器 中更新 “SOAPAction”， 在某些webservice中，可能不需要SOAPAction。

最后一步，就是将正确的SOAP消息粘贴到 消息体数据 文本框中。

## 6.2 添加用户

线程组，是告诉JMeter要模拟多并发用户数量和发送请求的频率。

## 6.3 添加监听器

监听器，负责将HTTP请求的所有结果进行可视化呈现和存储结果到文件。